### Usefull links

- Link to the paper: https://www.overleaf.com/9534664366kdxqgrmftmyv
- FAGCN paper: https://www.overleaf.com/8851228923xmygxjfkvjjq
- [OHC paper](./assets/HCO/Overlapping_Hierarchical_Clustering__IDA_2020___Camera_Ready_.pdf)

### Embedding alignment

- VecMap: https://github.com/artetxem/vecmap
- MUSE: https://github.com/facebookresearch/MUSE

### Graph alignment & Metrics

- https://www.dropbox.com/s/aehxbi83pv00jqy/SDM_2019_Tutorial_Bento.pdf?dl=0
