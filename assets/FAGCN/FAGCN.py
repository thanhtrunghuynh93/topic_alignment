# from algorithms.network_alignment_model import NetworkAlignmentModel

# from evaluation.metrics import get_statistics
from .embedding_model import FA_GCN, StableFactor
# from input.dataset import Dataset
# from utils.graph_utils import load_gt
import torch.nn.functional as F
import torch.nn as nn
# from FAGCN.utils import *
# from FAGCN.losses import *
# from sklearn.preprocessing import normalize
# from scipy.sparse import coo_matrix
# from scipy.sparse import csr_matrix

import torch
import numpy as np

from assets.OHC.lib.utils import to_print

import argparse
import os
import time
import sys

np.random.seed(7)
torch.manual_seed(7)

class FAGCN:
    """
    FAGCN model for networks alignment task
    """
    # def __init__(self, source_dataset, target_dataset, args):
    def __init__(self, source_feats, target_feats, source_A_hat, target_A_hat, num_GCN_blocks=2, num_epochs=20, learning_rate=0.01):
        """
        : source_graph, target_graph
        """
        self.source_feats = torch.FloatTensor(source_feats)
        self.target_feats = torch.FloatTensor(target_feats)

        self.source_A_hat = source_A_hat
        self.target_A_hat = target_A_hat
        self.num_epochs = num_epochs
        self.learning_rate = learning_rate
        self.num_GCN_blocks = num_GCN_blocks

        self.cuda = False
        if self.cuda:
            self.source_feats = self.source_feats.cuda()
            self.target_feats = self.target_feats.cuda()
            self.source_A_hat = self.source_A_hat.cuda()
            self.target_A_hat = self.target_A_hat.cuda()

        self.S = None


    def align(self, wid_container=None):
        source_A_hat = self.source_A_hat
        target_A_hat = self.target_A_hat
        source_feats = self.source_feats
        target_feats = self.target_feats
        
        embedding_model = FA_GCN(
            activate_function = "tanh",
            num_GCN_blocks = self.num_GCN_blocks,
            input_dim = 300,
            output_dim = 10,
            num_source_nodes = len(source_A_hat),
            num_target_nodes = len(target_A_hat),
            source_feats = source_feats,
            target_feats = target_feats
        )

        if self.cuda:
            embedding_model = embedding_model.cuda()

        embedding_model.train()

        structural_optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, embedding_model.parameters()), lr=self.learning_rate)
        self.train_embedding(embedding_model, source_A_hat, target_A_hat, structural_optimizer, wid_container=wid_container)
        return self.S


    def linkpred_loss(self, embedding, A):
        pred_adj = torch.matmul(F.normalize(embedding), F.normalize(embedding).t())
        if self.cuda:
            pred_adj = F.normalize((torch.min(pred_adj, torch.Tensor([1]).cuda())), dim = 1)
        else:
            pred_adj = F.normalize((torch.min(pred_adj, torch.Tensor([1]))), dim = 1)
        #linkpred_losss = (pred_adj - A[index]) ** 2
        A = A.to_dense()
        linkpred_losss = (pred_adj - A) ** 2
        linkpred_losss = linkpred_losss.sum() / A.shape[1]
        return linkpred_losss
    
    def linkpred_loss_multiple_layer(self, outputs, A_hat):
        count = 0 
        loss = 0
        for i in range(1, len(outputs)):
            loss += self.linkpred_loss(outputs[i], A_hat)
            count += 1
        loss = loss / count 
        return loss
    

    def train_embedding(self, embedding_model, source_A_hat, target_A_hat, structural_optimizer, wid_container=None):
        to_print("Do not use augmentation", wid_container)
        for epoch in range(self.num_epochs):
            to_print("Structure learning epoch: {}".format(epoch), wid_container)
            for i in range(2):
                structural_optimizer.zero_grad()
                if i == 0:
                    A_hat = source_A_hat
                    outputs = embedding_model(source_A_hat, 's')
                else:
                    A_hat = target_A_hat
                    outputs = embedding_model(target_A_hat, 't')
                loss = self.linkpred_loss_multiple_layer(outputs, A_hat)
                # if self.args.log:
                to_print("Loss: {:.4f}".format(loss.data), wid_container)
                loss.backward()
                structural_optimizer.step()
        self.get_simi_from_emb(embedding_model, source_A_hat, target_A_hat)


    def get_simi_from_emb(self, embedding_model, source_A_hat, target_A_hat):
        embedding_model.eval()
        source_outputs = embedding_model(source_A_hat, 's')
        target_outputs = embedding_model(target_A_hat, 't')
        # print("-"* 100)
        self.S = self.get_simi_matrix(source_outputs, target_outputs, [1, 1, 1])


    def get_simi_matrix(self, source_outputs, target_outputs, test_dict = None, alphas=None):
        Sf = np.zeros((len(source_outputs[0]), len(target_outputs[0])))
        for i in range(0, len(source_outputs)):
            S = torch.matmul(F.normalize(source_outputs[i]), F.normalize(target_outputs[i]).t())
            S_numpy = S.detach().cpu().numpy()
            if alphas is not None:
                Sf += alphas[i] * S_numpy
            else:
                Sf += S_numpy
        return Sf
    
