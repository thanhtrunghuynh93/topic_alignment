"""
visualisation_utils.py:
Contains useful functions that help with the visualisation at different stages of the phylomemetic structure
reconstruction.

Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>
Copyright (C) 2018 Diego Cardenas Cabeza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import sys
# External
import graphviz
import math
import ipywidgets as widgets
# Local files
from assets.OHC.lib import utils

########################
# Hierarchy management #
########################


def format_label(cluster_labels):
    # Transform the label to a correct form
    n = len(cluster_labels)
    k = math.ceil(math.sqrt(n))
    c_label = ""
    index = 0
    while index + k < n:
        c_label += str(cluster_labels[index:index + k]) + "\n"
        index += k
    c_label += str(cluster_labels[index:])
    return c_label


def node_color(dens_inf, dens_sup, density_color=None):
    if density_color:
        return "/rdylbu9/" + str(max(min(int(dens_inf * 10), 9), 1)) + \
               ":/rdylbu9/" + str(max(min(int(dens_sup * 10), 9), 1))
    else:
        return "white"


# NOTE the gradient fill is only available for cairo or svg rendering.
# See: https://graphviz.org/doc/info/attrs.html#k:colorList
def hierarchy_to_graph(hierarchy, top_levels=-1, labels=None, most_freq=None, density_color=True, distance_axe=True,
                       save=None, graph_format='svg', wid_container=None):
    """
    Create a representation of the hierarchy with a view focused on the changes of the clusters.
    -----
    :param hierarchy:       A list of hierarchy levels where each level is composed of 3 elements, the distance min and
                            the distance max for which the clusters don't change and the associated list of clusters
    :type hierarchy:        OHC
    :param top_levels:      Number of levels to display from the top of the hierarchy
    :param labels:          List of labels for the nodes
    :param most_freq:       Maximal number of words per topic to display (select the most frequent ones)
    :param density_color:   If true, color the nodes according to the evolution of their density. Else, keep them white
    :param distance_axe:    If true, display an axe of distance and the right of the graph
    :param save:            If a directory path is given, save the representation and
                            display it in an external window before returning it
    :param graph_format:    Format of the graphviz output. svg by default
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return the representation as a graphviz directed graph
    """
    # Check parameters
    if hierarchy.labels() is None:
        if labels is None:
            labels = []
    else:
        labels = hierarchy.labels()

    # Create the directed graph
    graph_hierarchy = graphviz.Digraph(name='.'.join(hierarchy.name().split('.')[:-1]), format=graph_format)
    graph_hierarchy.graph_attr['rankdir'] = 'BT'

    levels = hierarchy.levels()
    if top_levels > 0:
        levels = levels[len(hierarchy) - min(top_levels, len(hierarchy)):]

    level0 = levels[0]
    d_min0 = level0.d_min()
    d_max0 = level0.d_max()
    density_min0 = level0.density_min()
    density_max0 = level0.density_max()
    clusts_old = []

    # Add the cluster nodes of the leaves of the hierarchy to the graph
    for c, dens_inf, dens_sup in zip(level0.clusters(), density_min0, density_max0):
        # Add the node to the graph
        c_label = format_label(hierarchy.labels(c, most_freq))
        if most_freq and most_freq < len(c):
            c_label += "\n(" + str(len(c)) + ")"
        graph_hierarchy.node(str(c) + "_" + str(0), c_label, style="filled", gradientangle="90",
                             fillcolor=node_color(dens_inf, dens_sup, density_color))
        clusts_old.append(c)

    if distance_axe:
        if d_min0 == d_max0:
            graph_hierarchy.node(str(0), str(d_min0), shape='box')
        else:
            graph_hierarchy.node(str(0), str(d_min0) + " --> " + str(d_max0), shape='box')

    # Browse the hierarchical dictionary
    for i, level in utils.log_progress(enumerate(levels[1:]), every=1, name="Processed levels", wid_container=wid_container):
        d_min = level.d_min()
        d_max = level.d_max()
        density_min = level.density_min()
        density_max = level.density_max()
        clusts_new = level.clusters()

        # Create a subgraph for the distance d_inf
        with graph_hierarchy.subgraph(name='sub_' + str(i + 1)) as sub_graph_hierarchy:

            # Force the nodes to be at the same level
            sub_graph_hierarchy.graph_attr.update(rank='same')

            # Add the distance nodes and the link with the previous level
            if distance_axe:
                if d_min == d_max:
                    sub_graph_hierarchy.node(str(i + 1), str(d_min), shape='box')
                else:
                    sub_graph_hierarchy.node(str(i + 1), str(d_min) + " --> " + str(d_max), shape='box')
                graph_hierarchy.edge(str(i), str(i + 1), color='black')

            # Add the cluster nodes for the distance d_inf

            for c, dens_inf, dens_sup in zip(clusts_new, density_min, density_max):
                # Add the node to the sub graph
                c_label = format_label(hierarchy.labels(c, most_freq))
                if most_freq and most_freq < len(c):
                    c_label += "\n(" + str(len(c)) + ")"
                sub_graph_hierarchy.node(str(c) + "_" + str(i + 1), c_label, style="filled", gradientangle="90",
                                         fillcolor=node_color(dens_inf, dens_sup, density_color))

                try:
                    # Look if the current cluster already exists in the previous level of the hierarchy ie in clusts_old
                    index = clusts_old.index(c)
                except ValueError:
                    index = -1
                # If the cluster does not exist we need to detect more precisely the changes to avoid to link
                #  all the previous clusters that have a common part with this one.
                #  -> Idea: best Jaccard index then a bit less,
                #  until we recovered all the elements of the new clusters.
                if index == -1:
                    # Compute the Jaccard index for every previous cluster
                    jacc_sim = []
                    for c_old in clusts_old:
                        inter = set(c_old).intersection(c)
                        if inter:
                            w = float(len(inter)) / (len(c) + len(c_old) - len(inter))
                            jacc_sim.append((c_old, w))
                    jacc_sim = sorted(jacc_sim, key=lambda tup: tup[1], reverse=True)
                    # Determine the best ancestors
                    superposition = set()
                    c_old, sim = jacc_sim.pop(0)
                    # We stop when we totally cover c with the set of c_old that maximises the Jaccard index
                    while superposition != set(c):
                        # If the max Jaccard sim is the same for several clusters we take them all
                        sim_old = sim
                        while sim == sim_old:
                            # For each cluster c_old with the max remaining Jaccard similarity
                            # we add it to the superposition set
                            superposition.update(set(c_old).intersection(c))
                            # And add a link from c_old to c in the graph to display
                            # (solid for a subset and dashed otherwise)
                            if set(c_old).issubset(c):
                                style = "solid"
                            else:
                                style = "dashed"
                            # Draw the arrow with 6 color shades according to the jaccard index between the nodes.
                            graph_hierarchy.edge(str(c_old) + "_" + str(i), str(c) + "_" + str(i + 1), style=style,
                                                 color="9",
                                                 # weight=str(int(10*sim)), color=str(int((10 * sim) / 2) + 4),
                                                 colorscheme="greys9")
                            if not jacc_sim:
                                break
                            c_old, sim = jacc_sim.pop(0)

                else:
                    # If yes we add a link only to this previous cluster
                    graph_hierarchy.edge(str(c) + "_" + str(i), str(c) + "_" + str(i + 1),  # weight=str(10),
                                         color="9", colorscheme="greys9")
        clusts_old = clusts_new

    if save:
        output = graph_hierarchy.render(directory=save, view=True, cleanup=True)
        if wid_container:
            wid_container_content = list(wid_container.children)
            wid_container_content.append(widgets.HTML(value="Graph saved in " + output))
            wid_container.children = wid_container_content
        else:
            print("Graph saved in " + output)
    return graph_hierarchy
