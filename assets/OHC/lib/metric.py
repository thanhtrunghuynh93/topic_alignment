"""
metric.py:
Contains useful metric related functions.

Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>
Copyright (C) 2018 Diego Cardenas Cabeza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import operator
# External
import numpy as np
import pandas as pd
from scipy.cluster.hierarchy import cut_tree
from ast import literal_eval
import ipywidgets as widgets
from IPython.display import display
# Local files

# General configuration
logger = logging.getLogger()


def jaccard_similarity(a, b):
    """
    Compute the Jaccard similarity between 2 sets
    -----
    The Jaccard similarity between 2 sets a and b is defined by d(a,b) = |a ∩ b| / |a ∪ b|
    """
    a = set(a)
    b = set(b)
    c = a.intersection(b)
    return float(len(c)) / (len(a) + len(b) - len(c))


def jaccard_sim_clusters(clusters1, clusters2):
    """
    Return a matrix containing the jaccard similarity between a cluster c from clusters1 and a cluster c' from clusters2
    -----
    The Jaccard similarity between 2 clusters c and c' is defined by d(c,c') = |c ∩ c'| / |c ∪ c'|

    clusters1 and clusters2 should be a list of clusters, ie a list of lists of words.
    """
    links = []
    for c1 in clusters1:
        row = []
        for c2 in clusters2:
            row.append(jaccard_similarity(set(c1), set(c2)))
        links.append(row)
    return np.array(links)


# TODO define the sim using the Phi function as define by Chavalarias? How to make it symmetrical?
def cluster_matching(cluster, clusters):
    """
    Determine the best set of clusters that match with the considered cluster in terms of Jaccard similarity.
    -----
    :param cluster:     Cluster to match.
    :param clusters:    List of Clusters.
    :return:            The list of matching clusters and the associated similarity.
    """
    # Compute the Jaccard similarity for all the cluster of the matching cluster candidates
    cluster = set(cluster)
    sims = dict()
    for c in clusters:
        sims[frozenset(c)] = jaccard_similarity(cluster, set(c))
    # First we take the maximal sim and the corresponding cluster that we confirm as a match
    clust_union, sim_union = max(sims.items(), key=operator.itemgetter(1))
    # TODO return the same type of clusters as in parameter for the matching cluster list
    maching_clusters = [clust_union]
    del(sims[clust_union])
    # Then we look for the next best correspondancy and look if the union give a better sim or not.
    # If it is the case we add this cluster to the list of matchs otherwise we stop looking.
    while len(sims) > 0:
        clust_tmp = max(sims.items(), key=operator.itemgetter(1))[0]
        clust_union |= clust_tmp
        sim_tmp = jaccard_similarity(cluster, clust_union)
        if sim_tmp > sim_union:
            sim_union = sim_tmp
            maching_clusters.append(clust_tmp)
            del(sims[clust_tmp])
        else:
            break
    # Finally we return the list of matchs and the global sim
    return maching_clusters, sim_union


def average_clusters_similarity(clusters1, clusters2):
    df_jaccard = jaccard_sim_clusters(clusters1, clusters2)
    return df_jaccard.max(0).mean(), df_jaccard.max(1).mean()


def level_similarity(clusters1, clusters2, alignment=None, method='avg'):
    """
    Compute the similarity between 2 sets of clusters. It uses an alignment if given or the best Jaccard similarity to
    match the cluster otherwise and the total similarity is obtained using the given method in parameter.
    -----
    :param clusters1:       List of lists of elements that form the clusters (list of word clusters)
    :param clusters2:       List of lists of elements that form the clusters (list of word clusters)
    :param alignment:       Alignment args: id2clusters1, id2clusters2, aligned_dict
    :param method:          Method used to compute the level similarity (avg or weighted)
    :return:                Return a value of similarity as a float between 0 and 1
    """
    try:
        id2clusters1 = alignment["id2clusters1"]  # Mapping dictionary of the clusters1 (dict of word clusters)
        id2clusters2 = alignment["id2clusters2"]  # Mapping dictionary of the clusters2 (dict of word clusters)
        aligned_dict = alignment["aligned_dict"]  # Alignment clusters1 to clusters2 (dict of cluster ids)
    except (TypeError, KeyError):
        m_jaccard_sim = jaccard_sim_clusters(clusters1, clusters2)  # len(clusters1) x len(clusters2) similarity matrix
        sims1 = m_jaccard_sim.max(0)
        sims2 = m_jaccard_sim.max(1)
    else:
        sims1 = [jaccard_similarity(c1, id2clusters2[aligned_dict[id2clusters1.index(c1)]]) for c1 in clusters1]
        sims2 = []
        for c2 in clusters2:
            index_target = id2clusters2.index(c2)
            # Get all the node indices that point to the current cluster
            indices_source = [i for i, x in enumerate(list(aligned_dict.values())) if x == index_target]
            if len(indices_source) == 0:
                sims2.append(0)
            else:
                # Determine the most similar node
                sims2.append(np.max([jaccard_similarity(c2, id2clusters1[i]) for i in indices_source]))
    if method == 'avg':
        sim1 = np.mean(sims1)
        sim2 = np.mean(sims2)
    elif method == 'weighted':
        # Each cluster element as the same weight (if a word appears twice because of an overlap,
        # it will have in total the double weight of a word that appears only once)
        nb_elements1 = np.sum([len(c1) for c1 in clusters1])
        nb_elements2 = np.sum([len(c2) for c2 in clusters2])
        sim1 = np.sum([s1*len(c1) for s1, c1 in zip(sims1, clusters1)])/nb_elements1
        sim2 = np.sum([s2*len(c2) for s2, c2 in zip(sims2, clusters2)])/nb_elements2
    else:
        raise ValueError('Wrong method value')
    return (sim1 + sim2) / 2


def ohc_tree_similarity(ohc_hierarchy, scipy_tree_matrix):
    """
    Compute the similarity between an OHC hierarchy and a dendrogram represented as a scipy tree matrix.
    -----
    :param ohc_hierarchy:       An OHC object
    :param scipy_tree_matrix:   A scipy tree matrix
    :return:                    Return a value of similarity as a float between 0 and 1
    """
    similarity = 0
    scipy_clusters = []
    df_sim = None
    levels = ohc_hierarchy.levels()
    for level in levels:
        ohc_clusters = level.clusters()
        n = len(ohc_clusters)
        # Compute scipy_clusters only if the length of ohc_clusters has changed
        if len(scipy_clusters) != n:
            scipy_cutree = cut_tree(scipy_tree_matrix, n_clusters=n)
            scipy_clusters = [[] for _ in range(len(ohc_clusters))]
            for (i, v) in enumerate(scipy_cutree):
                scipy_clusters[v[0]].append(i)
        # Initialization
        if df_sim is None:
            df_sim = pd.DataFrame(jaccard_sim_clusters(ohc_clusters, scipy_clusters),
                                  columns=[str(c) for c in scipy_clusters])
            df_sim["index"] = [str(c) for c in ohc_clusters]
            df_sim = df_sim.set_index("index")
            df_sim.index.name = None
        else:
            # Determine the changed in ohc_clusters and scipy_clusters
            ohc_removed = set(df_sim.index) - set([str(c) for c in ohc_clusters])
            ohc_new = set([str(c) for c in ohc_clusters]) - set(df_sim.index)
            scipy_removed = set(df_sim.columns) - set([str(c) for c in scipy_clusters])
            scipy_new = set([str(c) for c in scipy_clusters]) - set(df_sim.columns)

            # Remove the old clusters from the similarity matrix
            df_sim.drop(scipy_removed, axis=1, inplace=True)
            df_sim.drop(ohc_removed, axis=0, inplace=True)

            # Compute the new similarities for the scipy_clusters
            for sn in scipy_new:
                sn_set = set(literal_eval(sn))
                df_sim[sn] = [jaccard_similarity(sn_set, set(literal_eval(c))) for c in df_sim.index]

            # Compute the new similarities for the ohc_clusters
            for ohcn in ohc_new:
                ohcn_set = set(literal_eval(ohcn))
                new_row = pd.Series(
                    data={c: jaccard_similarity(ohcn_set, set(literal_eval(c))) for c in df_sim.columns},
                    name=ohcn)

                df_sim = df_sim.append(new_row, ignore_index=False)

        sim1 = df_sim.max(0).mean()
        sim2 = df_sim.max(1).mean()
        similarity += (sim1 + sim2) / 2
    return similarity / len(ohc_hierarchy)


def tree_similarity(scipy_tree_matrix1, scipy_tree_matrix2):
    """
    Compute the similarity between two dendrogram represented as scipy tree matrices.
    -----
    :param scipy_tree_matrix1:  A scipy tree matrix
    :param scipy_tree_matrix2:  A scipy tree matrix
    :return:                    Return a value of similarity as a float between 0 and 1
    """
    similarity = 0
    df_sim = None
    n = scipy_tree_matrix1.shape[0]
    for i in range(n+1):
        # Clusters1
        scipy_cutree1 = cut_tree(scipy_tree_matrix1, n_clusters=i+1)
        scipy_clusters1 = [[] for _ in range(i+1)]
        for (j, v) in enumerate(scipy_cutree1):
            scipy_clusters1[v[0]].append(j)
        # Clusters2
        scipy_cutree2 = cut_tree(scipy_tree_matrix2, n_clusters=i+1)
        scipy_clusters2 = [[] for _ in range(i+1)]
        for (j, v) in enumerate(scipy_cutree2):
            scipy_clusters2[v[0]].append(j)
        # Initialization
        if df_sim is None:
            df_sim = pd.DataFrame(jaccard_sim_clusters(scipy_clusters1, scipy_clusters2),
                                  columns=[str(c) for c in scipy_clusters2])
            df_sim["index"] = [str(c) for c in scipy_clusters1]
            df_sim = df_sim.set_index("index")
            df_sim.index.name = None
        else:
            # Determine the changed in the 2 sets of scipy_clusters
            scipy_removed1 = set(df_sim.index) - set([str(c) for c in scipy_clusters1])
            scipy_new1 = set([str(c) for c in scipy_clusters1]) - set(df_sim.index)
            scipy_removed2 = set(df_sim.columns) - set([str(c) for c in scipy_clusters2])
            scipy_new2 = set([str(c) for c in scipy_clusters2]) - set(df_sim.columns)

            # Remove the old clusters from the similarity matrix
            df_sim.drop(scipy_removed2, axis=1, inplace=True)
            df_sim.drop(scipy_removed1, axis=0, inplace=True)

            # Compute the new similarities for the 2nd set of scipy_clusters
            for sn in scipy_new2:
                sn_set = set(literal_eval(sn))
                df_sim[sn] = [jaccard_similarity(sn_set, set(literal_eval(c))) for c in df_sim.index]

            # Compute the new similarities for the 1st set of scipy_clusters
            for sn in scipy_new1:
                sn_set = set(literal_eval(sn))
                new_row = pd.Series(
                    data={c: jaccard_similarity(sn_set, set(literal_eval(c))) for c in df_sim.columns},
                    name=sn)

                df_sim = df_sim.append(new_row, ignore_index=False)

        sim1 = df_sim.max(0).mean()
        sim2 = df_sim.max(1).mean()
        similarity += (sim1 + sim2) / 2
    return similarity/(n+1)


def ohc_similarity(ohc_hierarchy1, ohc_hierarchy2, top_k=-1, alignment=None, details=False, method='weighted',
                   wid_container=None):
    """
    Compute the similarity between 2 OHC structures as an average of similarities of corresponding levels.
    -----
    :param ohc_hierarchy1:  An OHC object
    :param ohc_hierarchy2:  An OHC object
    :param top_k:           Parameter to limit the similarity to the top k levels
    :param alignment:       Alignment arguments: id2clusters1, id2clusters2, aligned_dict
    :param details:         Indicate if we want to access to the corresponding level similarities
    :param method:          Method used to compute the level similarity (avg or weighted)
    :param wid_container:   Container where to display the progresses and results. Default to None = standard output
    :return:                Return a value of similarity as a float between 0 and 1 or a list of similarities and
                            its associated lists of indices of levels of ohc1 and ohc2
    """

    # Init variables #
    ##################

    sim_levels1 = []
    sim_levels2 = []
    sim_values = []
    levels1 = ohc_hierarchy1.levels()
    levels2 = ohc_hierarchy2.levels()
    depth1 = len(levels1)
    depth2 = len(levels2)

    # Compare only the top k levels of the hierarchies by setting the correct maximal depths to each hierarchy
    lowest_level1 = 0
    lowest_level2 = 0
    if top_k > 0:
        lowest_level1 = depth1 - min(top_k, depth1)
        lowest_level2 = depth2 - min(top_k, depth2)

    # Init widgets #
    ################

    name1 = "ohc_levels1"
    max_progress1 = depth1 - lowest_level1
    label1 = widgets.HTML(value=name1 + ": 0 / " + str(max_progress1))
    progress1 = widgets.IntProgress(min=0, max=max_progress1, value=0)
    every1 = 1 if max_progress1 <= 200 else int(max_progress1 / 200)
    box1 = widgets.VBox(children=[label1, progress1])
    name2 = "ohc_levels2"
    max_progress2 = depth2 - lowest_level2
    label2 = widgets.HTML(value=name2 + ": 0 / " + str(max_progress2))
    progress2 = widgets.IntProgress(min=0, max=max_progress2, value=0)
    every2 = 1 if max_progress2 <= 200 else int(max_progress2 / 200)
    box2 = widgets.VBox(children=[label2, progress2])
    box = widgets.HBox(children=[box1, box2])
    if wid_container:
        wid_container_content = list(wid_container.children)
        wid_container_content.append(box)
        wid_container.children = wid_container_content
    else:
        display(box)

    # Process the hierarchies #
    ###########################

    i1 = depth1 - 1  # Position in the hierarchy 1
    i2 = depth2 - 1  # Position in the hierarchy 2
    nb_clust_old = -1  # Number of clusters of the previous levels if common, -1 otherwise
    while i1 >= lowest_level1 and i2 >= lowest_level2:
        clusters1 = [set(ohc_hierarchy1.labels(c1)) for c1 in levels1[i1].clusters()]
        clusters2 = [set(ohc_hierarchy2.labels(c2)) for c2 in levels2[i2].clusters()]
        nb_clust1 = len(clusters1)
        nb_clust2 = len(clusters2)
        i1_tmp = i1
        i2_tmp = i2

        ##########
        # Different number of clusters #

        if nb_clust1 < nb_clust2:
            # We take the sim max between sim(i1(nb_clust1), i2(nb_clusts2=nb_clust1+1))
            #                         and sim(i1(nb_clusts1), i2+1(nb_clust2-2=nb_clust1-1))
            sim_tmp = level_similarity(clusters1, clusters2, alignment, method=method)
            if sim_levels2 and sim_levels2[-1] > i2 and i2 < depth2 - 1:
                clusters2_tmp = [set(ohc_hierarchy2.labels(c2_tmp)) for c2_tmp in levels2[i2 + 1].clusters()]
                sim_up = level_similarity(clusters1, clusters2_tmp, alignment, method=method)
                if sim_up > sim_tmp:
                    i2_tmp = i2 + 1
                    sim_tmp = sim_up
            sim_levels1.append(i1_tmp)
            sim_levels2.append(i2_tmp)
            sim_values.append(sim_tmp)
            # print(i1_tmp, i2_tmp, sim_tmp, "nb1<nb2")
            nb_clust_old = -1
            i1 -= 1
        elif nb_clust2 < nb_clust1:
            # We take the sim max between sim(i1(nb_clust1=nb_clust2+1), i2(nb_clusts2))
            # and sim(i1+1(nb_clusts1-2=nb_clust2-1), i2(nb_clust2))
            sim_tmp = level_similarity(clusters1, clusters2, alignment, method=method)
            if sim_levels1 and sim_levels1[-1] > i1 and i1 < depth1 - 1:
                clusters1_tmp = [set(ohc_hierarchy1.labels(c1_tmp)) for c1_tmp in levels1[i1+1].clusters()]
                sim_up = level_similarity(clusters1_tmp, clusters2, alignment, method=method)
                if sim_up > sim_tmp:
                    i1_tmp = i1 + 1
                    sim_tmp = sim_up
            sim_levels1.append(i1_tmp)
            sim_levels2.append(i2_tmp)
            sim_values.append(sim_tmp)
            # print(i1_tmp, i2_tmp, sim_tmp, "nb2<nb1")
            nb_clust_old = -1
            i2 -= 1
        else:
            ##########
            # Same number of clusters #

            nb_clust = nb_clust1
            # Check if it is the first time we find this number of clusters of not
            if nb_clust != nb_clust_old:
                sim_levels1.append(i1_tmp)
                sim_levels2.append(i2_tmp)
                sim_values.append(level_similarity(clusters1, clusters2, alignment, method=method))
                # print(i1_tmp, i2_tmp, sim_values[-1], "first nb2=nb1")

            sims_tmp = dict()

            if i1 > lowest_level1:
                if len(levels1[i1 - 1]) == nb_clust:
                    # If the level i1-1 has the same number of clusters than nb_clust
                    clusters1_tmp = [set(ohc_hierarchy1.labels(c1_tmp)) for c1_tmp in levels1[i1 - 1].clusters()]
                    sims_tmp[(i1 - 1, i2)] = level_similarity(clusters1_tmp, clusters2, alignment, method=method)

                    if i2 > lowest_level2:
                        if len(levels2[i2 - 1]) == nb_clust:
                            # If both levels i1-1 and i2-1 have the same numbers of clusters than nb_clust
                            clusters2_tmp = [set(ohc_hierarchy2.labels(c2_tmp)) for c2_tmp in
                                             levels2[i2 - 1].clusters()]
                            sims_tmp[(i1 - 1, i2 - 1)] = level_similarity(clusters1_tmp, clusters2_tmp, alignment,
                                                                          method=method)

            if i2 > lowest_level2:
                if len(levels2[i2 - 1]) == nb_clust:
                    # If the level i2-1 has the same number of clusters than nb_clust
                    clusters2_tmp = [set(ohc_hierarchy2.labels(c2_tmp)) for c2_tmp in levels2[i2 - 1].clusters()]
                    sims_tmp[(i1, i2 - 1)] = level_similarity(clusters1, clusters2_tmp, alignment, method=method)

            # Update nb_clust_old to the current value
            nb_clust_old = nb_clust
            # If necessary add a value to similarity and update i1 and i2
            if sims_tmp:
                # TODO deal when 2 sims of sims_tmp have the same max value
                arg_max = max(sims_tmp, key=sims_tmp.get)
                sim_levels1.append(arg_max[0])
                sim_levels2.append(arg_max[1])
                sim_values.append(sims_tmp[arg_max])
                # print(arg_max[0], arg_max[1], sims_tmp[arg_max], "other nb2=nb1")
                i1 = arg_max[0]
                i2 = arg_max[1]

            else:
                i1 -= 1
                i2 -= 1

        # Update widgets
        index1 = depth1 - 1 - i1
        if index1 == 1 or index1 % every1 == 0 or index1 == max_progress1:
            progress1.value = index1
            label1.value = name1 + ": " + str(index1) + " / " + str(max_progress1)
            if index1 == max_progress1:
                progress1.bar_style = 'success'
        index2 = depth2 - 1 - i2
        if index2 == 1 or index2 % every2 == 0 or index2 == max_progress2:
            progress2.value = index2
            label2.value = name2 + ": " + str(index2) + " / " + str(max_progress2)
            if index2 == max_progress2:
                progress2.bar_style = 'success'

    ##########
    # Different sizes of the vocab/number of clusters at the bottom of the hierarchies #

    # In the case where we stop because i2 < max_depth2 and i1 >= max_depth1
    clusters2 = [set(ohc_hierarchy2.labels(c2)) for c2 in levels2[lowest_level2].clusters()]
    while i1 >= lowest_level1:
        clusters1 = [set(ohc_hierarchy1.labels(c1)) for c1 in levels1[i1].clusters()]
        sim_levels1.append(i1)
        sim_levels2.append(lowest_level2)
        sim_values.append(level_similarity(clusters1, clusters2, alignment, method=method))
        # print(i1, lowest_level2, sim_values[-1], "lowest_level2")
        i1 -= 1

        # Update widgets
        index1 = depth1 - 1 - i1
        if index1 == 1 or index1 % every1 == 0 or index1 == max_progress1:
            progress1.value = index1
            label1.value = name1 + ": " + str(index1) + " / " + str(max_progress1)
            if index1 == max_progress1:
                progress1.bar_style = 'success'

    # In the case where we stop because i1 < max_depth1 and i2 >= max_depth2
    clusters1 = [set(ohc_hierarchy1.labels(c1)) for c1 in levels1[lowest_level1].clusters()]
    while i2 >= lowest_level2:
        clusters2 = [set(ohc_hierarchy2.labels(c2)) for c2 in levels2[i2].clusters()]
        sim_levels1.append(lowest_level1)
        sim_levels2.append(i2)
        sim_values.append(level_similarity(clusters1, clusters2, alignment, method=method))
        # print(lowest_level1, i2, sim_values[-1], "lowest_level1")
        i2 -= 1

        # Update widgets
        index2 = depth2 - 1 - i2
        if index2 == 1 or index2 % every2 == 0 or index2 == max_progress2:
            progress2.value = index2
            label2.value = name2 + ": " + str(index2) + " / " + str(max_progress2)
            if index2 == max_progress2:
                progress2.bar_style = 'success'

    if details:
        return sim_levels1, sim_levels2, sim_values
    else:
        return np.mean(sim_values)


def accuracy(cluster_labels1, cluster_labels2):
    n = min(len(cluster_labels1), len(cluster_labels2))
    nb_correct_answer = 0
    for i in range(n):
        if cluster_labels1[i] == cluster_labels2[i]:
            nb_correct_answer += 1
    return float(nb_correct_answer)/n
