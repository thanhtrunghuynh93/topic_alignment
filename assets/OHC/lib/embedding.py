"""
embedding.py:
Contains useful functions that help to build embedding models.•

Copyright (C) 2019-2020 Ian Jeantet <ian.jeantet@irisa.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import os
import random
import multiprocessing
# External
import pandas as pd
import gensim
import unidecode
# Local files
from assets.OHC.lib import utils  # get_param, update_param, get_distance

# General configuration
logger = logging.getLogger()

# TODO create and use a model object?
# TODO Replace all the os.walk by os.listdir

################
# Model object #
################


class Model(object):
    def __init__(self, model_file, sort=True):
        self.__vectors = load_model_from_csv(model_file, sort)
        year = utils.get_param(model_file, param="year")
        if year:
            self.__year = int(year[0])
        else:
            self.__year = -1

####################
# Model management #
####################


def generate_embeddings(data_path, models_path, opt=4, period_size=1, period=None, step=1, stop_word_file=None,
                        embedding_size=300, learning_rate=0.025, window_size=2, min_count=10, subsample=1e-3,
                        workers=multiprocessing.cpu_count(), sg=True, num_samples=5, epochs_to_train=5, batch_size=100,
                        max_final_vocab=None, fixed_vocab=None):
    """
    Deternime a vocabulary and generate the corresponding embeddings using gensim SGNS technique.
    -----
    For the gensim word2vec parameters, please look at the doc here: see the doc here:
    https://radimrehurek.com/gensim/models/word2vec.html#gensim.models.word2vec.Word2Vec
    :param data_path:       Folder containing the files to process
    :param models_path:     Path where to store the generated models
    :param opt:             Vocabulary option in {1:previous, 2:union, 3:intersection, 4:new, 5:fixed}
    :param period_size:     Size of the time window over the data (in terms of number of files for one period)
    :param period:          Period parameter to build correct file names ("year", "publication_year", "month"...)
    :param step:            Indicate how to slide the time window over the files
    :param stop_word_file:  File containing a set of stop words to ignore
    :param embedding_size:  Size of the embeddings
    :param learning_rate:   Gensim word2vec parameter
    :param window_size:     Gensim word2vec parameter
    :param min_count:       Gensim word2vec parameter
    :param subsample:       Gensim word2vec parameter
    :param workers:         Gensim word2vec parameter
    :param sg:              Gensim word2vec parameter
    :param num_samples:     Gensim word2vec parameter
    :param epochs_to_train: Gensim word2vec parameter
    :param batch_size:      Gensim word2vec parameter
    :param max_final_vocab  Gensim word2vec parameter
    :param fixed_vocab      List of key words to use as vocab
    :return:                None
    """

    nb_processed_files = 0
    nb_models = 0
    nb_empty_files = 0
    nb_empty_vocabs = 0
    previous_model = None
    previous_model_file = ""

    # Processing
    if not 0 < opt <= 5:
        print('Please chose a correct vocabulary option in {1:previous, 2:union, 3:intersection, 4:new, 5:fixed}')
    else:

        if fixed_vocab is None:
            fixed_vocab = dict()

        # Files path management (if data_path is a single file or a folder)
        files = []
        dir_path = ''
        if os.path.isdir(data_path):
            files = os.listdir(data_path)
            dir_path = data_path
        elif os.path.isfile(data_path):
            files.append(data_path.split('/')[-1])
            dir_path = '/'.join(data_path.split('/')[:-1])

        # Saving path management
        if not os.path.exists(models_path):
            os.makedirs(models_path)

        # Stop words management
        stop_list = set()
        if stop_word_file:
            with open(stop_word_file, 'r') as stop_words:
                stop_list = set(stop_words.read().split('\n'))

        files = sorted(files)
        nb_files = len(files)
        print(str(len(files)) + " files found")

        start = 0
        sentences_tab = []
        while start + period_size < nb_files:
            for file_name in files[start+len(sentences_tab):start+period_size]:
                if file_name.endswith(".txt"):
                    nb_processed_files += 1
                    # Checking if the file is empty or not
                    if os.stat(os.path.join(dir_path, file_name)).st_size == 0:
                        logger.info(file_name + " was ignored: empty file")
                        nb_empty_files += 1
                    else:
                        print("Processing " + file_name)

                        # Format the data of the file
                        sentences_tmp = []
                        for line in open(os.path.join(dir_path, file_name), 'r'):
                            sentences_tmp.append([w for w in line.strip().split(" ") if
                                                  w not in stop_list])  # Remove the stop-words from stop_list
                        sentences_tab.append(sentences_tmp)

            # Merge the data of the periods of the window
            sentences = []
            for s in sentences_tab:
                sentences += s

            # Build an empty model with the correct hyperparameters
            model = gensim.models.Word2Vec(size=embedding_size, alpha=learning_rate, window=window_size,
                                           min_count=min_count, sample=subsample, workers=workers, sg=sg,
                                           negative=num_samples, iter=epochs_to_train, batch_words=batch_size,
                                           max_final_vocab=max_final_vocab)

            # First model vocab computation
            if not previous_model and opt <= 4:
                model.build_vocab(sentences)

            # Other model vocab computation
            else:
                # Vocabulary option
                if opt == 1:
                    # 1 Replacement of the vocab
                    # We need to convert the words of the previous model to a dict before passing
                    # them to the function
                    model.build_vocab_from_freq({str(word): vocab_obj.count for word, vocab_obj in
                                                 previous_model.vocab.items()}, update=False)
                elif opt == 2:
                    # 2 Union of the dictionaries
                    model.build_vocab(sentences)
                    # Add the new words from the previous model to the vocab to the current model
                    # (we need to convert them as a dict to use the function bellow)
                    if model.wv.vocab:
                        model.build_vocab_from_freq(
                            {str(word): vocab_obj.count for word, vocab_obj in
                             previous_model.vocab.items()}, update=True)
                    else:
                        model.build_vocab_from_freq(
                            {str(word): vocab_obj.count for word, vocab_obj in
                             previous_model.vocab.items()}, update=False)
                elif opt == 3:
                    # 3 Intersection of the dictionaries
                    model_tmp = gensim.models.Word2Vec(size=embedding_size, alpha=learning_rate,
                                                       window=window_size, min_count=min_count,
                                                       sample=subsample, workers=workers, sg=sg,
                                                       negative=num_samples, iter=epochs_to_train,
                                                       batch_words=batch_size, max_final_vocab=max_final_vocab)
                    model_tmp.build_vocab(sentences)
                    vocab_tmp = set(model_tmp.wv.vocab) & set(previous_model.vocab)
                    if not vocab_tmp:  # If no word in common we use the vocab of the new model
                        vocab_tmp = set(model_tmp.wv.vocab)
                    model.build_vocab_from_freq(
                        {str(word): vocab_obj.count for word, vocab_obj in model_tmp.wv.vocab.items()
                         if word in vocab_tmp}, update=False)
                elif opt == 5:
                    # 5 intersection of the vocab with the fixed one given in parameter
                    model_tmp = gensim.models.Word2Vec(size=embedding_size, alpha=learning_rate,
                                                       window=window_size, min_count=5,
                                                       sample=subsample, workers=workers, sg=sg,
                                                       negative=num_samples, iter=epochs_to_train,
                                                       batch_words=batch_size, max_final_vocab=None)
                    model_tmp.build_vocab(sentences)
                    # print(len(model_tmp.wv.vocab))
                    vocab_tmp = set(model_tmp.wv.vocab) & set(fixed_vocab)
                    # print(vocab_tmp)
                    if not vocab_tmp:  # If no word in common we use the vocab of the new model
                        vocab_tmp = set(model_tmp.wv.vocab)
                    model.build_vocab_from_freq(
                        {str(word): vocab_obj.count for word, vocab_obj in model_tmp.wv.vocab.items()
                         if word in vocab_tmp}, update=False)
                else:  # opt == 4
                    # 4 Only vocabulary from the 2nd file
                    model.build_vocab(sentences)
                logger.info(str(len(model.wv.vocab)) + " words in the vocabulary of the 2nd model")

            model_name = files[start+period_size-1]
            if period:
                period_start = utils.get_param(files[start], period)
                period_stop = utils.get_param(files[start+period_size-1], period)
                if period_start != period_stop:
                    model_name = utils.update_param(files[start], period, period_start + period_stop)

            # Ignore if the vocab is empty
            if not model.wv.vocab:
                nb_empty_vocabs += 1
                logger.info(model_name + " was ignored: empty vocab")

            # else update the word vectors, train the model and save it
            else:
                if previous_model_file:
                    # Updating positions of the vectors of the words present in the model1
                    model.intersect_word2vec_format(previous_model_file, binary=False,
                                                    lockf=1.0)

                # Training
                training_examples_count = len(sentences)
                logger.info(str(training_examples_count) + " data will be used for the training")
                model.train(sentences, total_examples=training_examples_count, epochs=model.epochs)
                logger.info("Model trained")

                # Save the word vectors of the model
                # Save also gensim format model as 'previous_model_file' to update the words vectors
                # of the following model
                save_model_to_csv(model_name, models_path, model)
                nb_models += 1
                if previous_model_file:
                    os.remove(previous_model_file)
                previous_model_file = save_model(model_name, "/tmp/", model, binary_model=False)

                previous_model = model.wv
                logger.info("Model saved")

            if step < len(sentences_tab):
                sentences_tab = sentences_tab[step:]
            else:
                sentences_tab = []
            start += step

        print(str(nb_processed_files) + " files with the good criteria processed (including " + str(nb_empty_files)
              + " empty files and " + str(nb_empty_vocabs) + " empty vocabs)")
        print(str(nb_models) + " models generated")


def save_model(file_name, save_path, model, binary_model=True):
    """Saves the word vectors of the model"""
    # Preconditions
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    # Processing
    output_file_prefix = os.path.join(save_path, '.'.join(file_name.split('/')[-1].split('.')[:-1]))

    if binary_model:  # Save a binary format model
        output_file_name = output_file_prefix + ".word2vec.bin"
    else:  # Save a text format model
        output_file_name = output_file_prefix + ".word2vec.wv"

    model.wv.save_word2vec_format(output_file_name, binary=binary_model)

    output_vocab_name = output_file_prefix + ".vocab.txt"
    with open(output_vocab_name, 'w') as vocab_file:
        for word, vocab_obj in model.wv.vocab.items():
            vocab_file.write(word + "," + str(vocab_obj.count) + "\n")

    return output_file_name


def save_model_to_csv(file_name, save_path, model):
    """Writes a csv file containing the words, their word vectors and their frequency"""
    # Preconditions
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    # Processing
    output_file_prefix = os.path.join(save_path, '.'.join(file_name.split('/')[-1].split('.')[:-1]))
    output_file_name = output_file_prefix + ".word2vec.csv"

    with open(output_file_name, 'w') as csv_file:

        # Write headers
        fieldnames = ['Word']
        for i in range(len(model.wv[list(model.wv.vocab)[0]])):
            fieldnames.append(str(i))
        fieldnames.append('Frequency')
        csv_file.write(",".join(fieldnames) + "\n")

        # Write data
        for word, vocab_obj in model.wv.vocab.items():
            vector = model.wv[word]
            # str_vector = ",".join(list(vector))
            csv_file.write(word + "," + str(list(vector)).strip('[]') + "," + str(vocab_obj.count) + "\n")
    nb_word_vectors = len(model.wv.vocab.items())
    print(output_file_name + " saved (" + str(nb_word_vectors) + " word vectors generated)")
    return output_file_name


def generate_models_path(suffix_path, min_count, as_csv, opt=-1, first_model_year=-1):
    if min_count:
        suffix_path += "/min_count_" + str(min_count)
    if 0 < opt <= 4:
        suffix_path += "/opt" + str(opt)
    if as_csv:
        suffix_path += "/csv"
    else:
        suffix_path += "/gensim"
    if first_model_year <= 0:
        suffix_path += "/all_years"
    else:
        suffix_path += "/from_" + str(first_model_year)
    return suffix_path


def load_model_from_csv(file_name, sort=True):
    """
    Load a model from a csv file into a dataframe
    -----
    :param file_name:   Model file to load
    :param sort:        Optional, if true the dataframe is sorted according to the word frequency
    :return:            Return the dataframe
    """
    df = pd.read_csv(file_name, sep=',', header=0, converters={'Word': str})  # index_col=0,
    df = df.set_index('Word')
    if sort:
        df = df.sort_values(by=['Frequency'], ascending=False)
    return df


def get_vocab(data, min_count=20, max_final_vocab=None, contracted_words=None, stop_words=None):

    if contracted_words is None:
        contracted_words = dict()
    if stop_words is None:
        stop_words = []

    # Clean the text #
    ##################

    space = " "
    token_min_len = 2
    token_max_len = 15
    clean_lines = []
    for line in data:
        # Deal with the contracted forms of words like "it's" or "shouldn't"
        cw_line = []
        for token in line.split(' '):
            token = token.lower()
            if "'" in token:
                for cw in contracted_words.keys():
                    token = token.replace(cw, contracted_words[cw])
            cw_line.append(token)
        line = space.join(cw_line)
        # Tokenize and clean the line
        tok_line = [token for token in gensim.utils.simple_preprocess(line, deacc=True,
                                                                      min_len=token_min_len,
                                                                      max_len=token_max_len)
                    ]
        clean_lines.append(unidecode.unidecode(space.join(tok_line)))
    print("Data cleaned")

    # Transform the data into 3-grams #
    ###################################

    # Default parameter values
    min_count_ngram = 5
    threshold = 10

    # Parse the data for the Phraser
    clean_lines = [line.split(space) for line in clean_lines]

    # Compute 2-grams
    phrases = gensim.models.phrases.Phrases(clean_lines, min_count=min_count_ngram, threshold=threshold,
                                            common_terms=stop_words)
    ngram = gensim.models.phrases.Phraser(phrases)
    sentences = list(ngram[clean_lines])
    print("2-grams generated")

    # Compute 3-grams
    phrases = gensim.models.phrases.Phrases(sentences, min_count=min_count_ngram, threshold=threshold,
                                            common_terms=stop_words)
    ngram = gensim.models.phrases.Phraser(phrases)
    sentences = list(ngram[sentences])

    ngram_lines = [unidecode.unidecode(space.join(text)) for text in sentences]
    print("3-grams generated")

    # Compute the vocab #
    #####################

    # Format the data
    sentences = []
    for line in ngram_lines:
        sentences.append([w for w in line.strip().split(space) if
                          w not in stop_words])  # Remove the stop-words from stop_list

    # Build an empty model with the correct hyperparameters
    # min_count = 20
    # max_final_vocab = None
    model = gensim.models.Word2Vec(min_count=min_count, max_final_vocab=max_final_vocab)
    model.build_vocab(sentences)

    print(str(len(model.wv.vocab)) + " words in the vocab")

    vocab = pd.DataFrame([[word, vocab_obj.count] for word, vocab_obj in model.wv.vocab.items()],
                         columns=["word", "frequency"])
    vocab = vocab.sort_values(by=['frequency'], ascending=False)

    return vocab


def get_domain(model, word=None, n=10):
    if not word:
        w = random.randint(1, len(model.vocab))
        word = list(model.vocab)[w]
    similar_words = [word]
    for wo, sim in model.similar_by_word(word, n):
        similar_words.append(wo)
    print("domain_" + word + "=" + str(similar_words))
    return similar_words

##################
# Model analysis #
##################


def get_closest_words(df_word_vectors, w, n, dist_type="cosine"):
    """
    Return a list of the n closest words of w among all the words of the model
    -----
    Use the naive approach to determine the n closest vectors to a given one by computing all
    the distances and keeping only the n smallest

    Use the distance 'dist_type' given in parameter
    """
    # Get the vector corresponding to w to compute the distances
    w_vector = df_word_vectors.loc[w]

    # Create a new dataframe with the same number of lines
    d_wi_w = pd.DataFrame(index=df_word_vectors.index)

    # Add a new colunm with the distance between wi and the given word w
    d_wi_w['d'] = [utils.get_distance(w_vector, df_word_vectors.loc[i], dist_type) for i in df_word_vectors.index]

    # Sort the dataframe by distance
    d_wi_w = d_wi_w.sort_values(by=['d'])

    # Return only the n first indices (which correspond to the smallest distances)
    # => Note that if there are less than n words in the vocabulary it will not raise an error but return the maximum
    # possible values (ie the whole vocabulary)
    return d_wi_w.index.tolist()[1:n+1]


def get_words_closer_than_d(df_word_vectors, w, d, dist_type="cosine"):
    """
    Return a list of the words closer than d of w among all the words of the model
    -----
    Use the naive approach by computing all the distances and keeping only the
    smaller than d

    Use the distance 'dist_type' given in parameter
    """
    # Get the vector corresponding to w to compute the distances
    w_vector = df_word_vectors.loc[w]

    # Create a new dataframe with the same number of lines
    d_wi_w = pd.DataFrame(index=df_word_vectors.index)

    # Add a new colunm with the distance between wi and the given word w
    d_wi_w['d'] = [utils.get_distance(w_vector, df_word_vectors.loc[i], dist_type) for i in df_word_vectors.index]

    # Slice the dataframe according to d
    d_wi_w = d_wi_w[d_wi_w['d'] < d]

    # Sort the dataframe by distance
    d_wi_w = d_wi_w.sort_values(by=['d'])

    # w should be the first element of the list so it's removed from the result.
    return d_wi_w.index.tolist()[1:]


@DeprecationWarning
def get_barycenter(df_vectors):
    return [df_vectors[c].mean() for c in list(df_vectors)]


# Load the entire model so it takes more time than looking only for the given words
# (same time than loading the model in the worst case)
@DeprecationWarning
def get_relative_distances_old(root, models_path, w1, w2, distance="cosine"):
    """
    Return the relative distances between 2 given words (w1 and w2) for each model of the given folder (model_path)
    -----
    By default root should be the same string as model_path
    """
    distances = dict()
    for dirpath, subdirs, files in os.walk(models_path):
        logger.info("in " + dirpath)
        nb_models = 0
        model_found = False
        dir_label = "." + "".join(dirpath.rsplit(root))
        dists = dict()
        for file_name in files:
            if file_name.endswith(".word2vec.csv"):
                model_found = True
                nb_models += 1
                logger.info("Processing " + file_name)
                year = file_name.split('/')[-1].split(b'.')[-3]
                df_model = load_model_from_csv(os.path.join(dirpath, file_name))
                df_labels = df_model.index.tolist()  # Extract the labels (the words) of the df
                df_model = df_model.iloc[:, 0:-1]  # Remove the 'Frequency' column (the last one)
                if w1 in df_labels and w2 in df_labels:  # Verify that the 2 words are in the vocab
                    dists[int(year)] = utils.get_distance(df_model.loc[w1], df_model.loc[w2], distance)
                if not nb_models % 10:
                    logger.info(str(nb_models) + " models processed")
        if model_found:
            if dists:
                distances[dir_label] = dists
            logger.info(dir_label + " processed (total of " + str(nb_models) + " models)")
    return distances


def get_relative_distances(root, models_path, w1, w2, dist_type="cosine"):
    """
    Return the relative distances between 2 given words (w1 and w2) for each model of the given folder (model_path)
    -----
    By default root should be the same string as model_path
    """
    distances = dict()
    for dirpath, subdirs, files in os.walk(models_path):
        logger.info("in " + dirpath)
        nb_models = 0
        model_found = False
        dir_label = "." + "".join(dirpath.rsplit(root))
        dists = dict()
        for file_name in files:
            if file_name.endswith(".word2vec.csv"):
                model_found = True
                nb_models += 1
                logger.info("Processing " + file_name)
                year = utils.get_param(file_name, 'year')  # Extract the year from the name of the file
                try:
                    year = int(year[0])
                    w1_vector = None
                    w2_vector = None
                    for line in open(os.path.join(dirpath, file_name)):
                        w_tmp = line.split(',')[0]
                        if w_tmp == w1:
                            w1_vector = [float(i) for i in line.split(',')[1:-1]]
                            if w2_vector:
                                break
                        elif w_tmp == w2:
                            w2_vector = [float(i) for i in line.split(',')[1:-1]]
                            if w1_vector:
                                break
                    if w1_vector and w2_vector:  # Verify that the 2 words are in the vocab
                        dists[int(year)] = utils.get_distance(w1_vector, w2_vector, dist_type)
                except (IndexError, ValueError):
                    logging.warning(file_name + ": wrong year format in the file name.")
                if not nb_models % 10:
                    print(str(nb_models) + " models processed")
        if model_found:
            if dists:
                distances[dir_label] = dists
            print(dir_label + " processed (total of " + str(nb_models) + " models)")
    return distances


def get_relative_distances_estimation(root, models_path, w1, w2, n, dist_type="cosine"):
    """
    Return the relative distances between 2 given words (w1 and w2) for each model of the given folder (model_path)
    -----
    Try to estimate the relative distance when w1 and/or w2 is/are missing in a given model by taking the barycenter of
    the n closest words of the missing word(s). These n closest words are taken from the last model in which w1 or w2
    was.

    By default root should be the same string as model_path
    """
    distances = dict()
    result = ["real", "weak_estimation", "strong_estimation"]

    for dirpath, subdirs, files in os.walk(models_path):

        logger.info("in " + dirpath)
        nb_models = 0
        model_found = False
        dir_label = "." + "".join(dirpath.rsplit(root))
        dists = dict()
        closest_words_w1 = []
        closest_words_w2 = []

        for file_name in sorted(files):
            if file_name.endswith(".word2vec.csv"):
                model_found = True
                nb_models += 1
                logger.info("Processing " + file_name)
                year = utils.get_param(file_name, 'year')  # Extract the year from the name of the file
                try:
                    year = int(year[0])
                    w1_vector = []
                    w2_vector = []
                    r = 0
                    # Load the model
                    df_model = load_model_from_csv(os.path.join(dirpath, file_name))
                    df_labels = df_model.index.tolist()  # Extract the labels (the words) of the df
                    df_model = df_model.iloc[:, 0:-1]  # Remove the 'Frequency' column (the last one)
                    # Update the closest word sets if possible
                    if w1 in df_labels:
                        # print("w1 in df_labels")
                        closest_words_w1 = get_closest_words(df_model, w1, n, dist_type)
                        w1_vector = list(df_model.loc[w1])
                        # print("OK")
                    elif closest_words_w1:
                        # print("w1 not in df_labels but closest_words_w1")
                        w1_vector = list(get_barycenter(df_model.loc[[w for w in closest_words_w1 if w in df_labels]]))
                        r += 1
                        # print("OK")
                    if w2 in df_labels:
                        # print("w2 in df_labels")
                        closest_words_w2 = get_closest_words(df_model, w2, n, dist_type)
                        w2_vector = list(df_model.loc[w2])
                        # print("OK")
                    elif closest_words_w2:
                        # print("w2 not in df_labels but closest_words_w2")
                        w2_vector = list(get_barycenter(df_model.loc[[w for w in closest_words_w2 if w in df_labels]]))
                        r += 1
                        # print("OK")
                    # Compute the relative distance if possible
                    if w1_vector and w2_vector:
                        # dists[int(year)] = [utils.get_distance(w1_vector, w2_vector, dist_type), result[r]]
                        dists[int(year)] = [utils.get_distance(w1_vector, w2_vector, dist_type), r]
                        print(str(year) + ": " + str(dists[int(year)][0]) + " " + result[r])
                except (IndexError, ValueError):
                    logging.warning(file_name + ": wrong year format in the file name.")
                if not nb_models % 10:
                    print(str(nb_models) + " models processed")
        if model_found:
            if dists:
                distances[dir_label] = dists
            print(dir_label + " processed (total of " + str(nb_models) + " models)")
    return distances


def get_neighbours_k(root, models_path, w, k=10, dist_type="cosine"):
    """
    Return the k nearest neighbours of the given word w for each model of the given folder (model_path)
    -----
    By default root should be the same string as model_path

    The result is a dictionnary of dataframes where each dataframe contains the knns for a subfolder of
    model_path and each column of this dataframe is the knns for a specific year given as header of the column
    """
    neighbours = dict()
    for dirpath, subdirs, files in os.walk(models_path):
        logger.info("in " + dirpath)
        nb_models = 0
        model_found = False
        dir_label = "." + "".join(dirpath.rsplit(root))
        neighbs = pd.DataFrame(index=[i for i in range(1, k+1)])
        for file_name in files:
            if file_name.endswith(".word2vec.csv"):
                model_found = True
                nb_models += 1
                logger.info("Processing " + file_name)
                year = utils.get_param(file_name, 'year')  # Extract the year from the name of the file
                try:
                    year = int(year[0])
                    df_model = load_model_from_csv(os.path.join(dirpath, file_name))
                    df_labels = df_model.index.tolist()  # Extract the labels (the words) of the df
                    df_model = df_model.iloc[:, 0:-1]  # Remove the 'Frequency' column (the last one)
                    if w in df_labels:
                        neighbs[int(year)] = get_closest_words(df_model, w, k, dist_type)
                        # print(year + ": " + str(neighbs[int(year)].tolist()))
                except (IndexError, ValueError):
                    logging.warning(file_name + ": wrong year format in the file name.")
                if not nb_models % 10:
                    print(str(nb_models) + " models processed")
        if model_found:
            # Check that there is at least one column (ie one entry) in the DataFrame
            if not neighbs.empty:
                neighbours[dir_label] = neighbs.reindex(sorted(neighbs.columns), axis=1)
            print(dir_label + " processed ('" + w + "' appeard in " + str(neighbs.shape[1]) + " out of " +
                  str(nb_models) + " models)")

    return neighbours


def get_neighbours_d(root, models_path, w, d, dist_type="cosine"):
    """
    Return the words closer than d of the given word w for each model of the given folder (model_path)
    -----
    By default root should be the same string as model_path

    The result is a dictionary of dataframes where each dataframe contains the knns for a subfolder of
    model_path and each column of this dataframe is the knns for a specific year given as header of the column
    """
    neighbours = dict()
    for dirpath, subdirs, files in os.walk(models_path):
        logger.info("in " + dirpath)
        nb_models = 0
        w_in_model = 0
        model_found = False
        dir_label = "." + "".join(dirpath.rsplit(root))
        neighbs = dict()
        for file_name in files:
            if file_name.endswith(".word2vec.csv"):
                model_found = True
                nb_models += 1
                logger.info("Processing " + file_name)
                year = utils.get_param(file_name, 'year')  # Extract the year from the name of the file
                try:
                    year = int(year[0])
                    df_model = load_model_from_csv(os.path.join(dirpath, file_name))
                    df_labels = df_model.index.tolist()  # Extract the labels (the words) of the df
                    df_model = df_model.iloc[:, 0:-1]  # Remove the 'Frequency' column (the last one)
                    if w in df_labels:
                        w_in_model += 1
                        close_words = get_words_closer_than_d(df_model, w, d, dist_type)
                        if close_words:
                            neighbs[int(year)] = close_words
                except (IndexError, ValueError):
                    logging.warning(file_name + ": wrong year format in the file name.")
                if not nb_models % 10:
                    print(str(nb_models) + " models processed")
        if model_found:
            # Check that there is at least one column (ie one entry) in the DataFrame
            if neighbs:
                neighbours[dir_label] = neighbs  # .reindex(sorted(neighbs.columns), axis=1)
            print(dir_label + " processed ('" + w + "' appeard in " + str(w_in_model) + " out of "
                  + str(nb_models) + " models and neighbours closer than " + str(d) + " were found in "
                  + str(len(neighbs)) + " case(s))")

    return neighbours

#########################
# Vocabulary management #
#########################


def aggregate_dict_from_txt_vocab(vocab_path):
    """
    Aggregate the dictionaries from the txt vocab files of the given folder
    -----
    If a word appears in several dictionaries, the frequencies are summed
    :param vocab_path:  Path of the txt vocab files
    :return:            Return a dataframe containing the aggregated dictionary
    """
    df_vocab = pd.DataFrame(columns=['Word', 'Frequency'])
    for file_name in os.listdir(vocab_path):
        if file_name.endswith(".vocab.txt"):
            logger.info("Processing " + file_name)
            # Load current txt vocab file
            df_tmp = pd.read_csv(os.path.join(vocab_path, file_name), sep=',', names=['Word', 'Frequency'],
                                 index_col=0)
            # Concatenate the current df with the global one by summing the common word frequency
            df_vocab = pd.concat([df_vocab, df_tmp], sort=False).groupby('Word').sum()
    return df_vocab


def aggregate_dict_from_csv_model(models_path):
    """
    Aggregate the dictionaries from the csv models of the given folder
    -----
    If a word appears in several dictionaries, the frequencies are summed
    :param models_path: Path of the csv models
    :return:            Return a dataframe containing the aggregated dictionary
    """
    df_vocab = pd.DataFrame(columns=['Word', 'Frequency'])
    nb_models = 0
    for file_name in os.listdir(models_path):
        if file_name.endswith(".word2vec.csv"):
            nb_models += 1
            logger.info("Processing " + file_name)
            # Load the current csv model and slice it to keep only the 'Frequency' column
            df_tmp = load_model_from_csv(os.path.join(models_path, file_name), sort=False)['Frequency'].to_frame()
            # Concatenate the current df with the global one by summing the common word frequency
            df_vocab = pd.concat([df_vocab, df_tmp], sort=False).groupby('Word').sum()

            if not nb_models % 10:
                print(str(nb_models) + " models processed")
    print(str(nb_models) + " models processed")
    return df_vocab


def random_word_from_dict(dict_file, max_vocab_size=None, freq_min=-1):
    """
    Return a random word of a dictionary file given in parameter
    -----
    :param dict_file:       Any dictionary file in the format one line = word + frequency
    :param max_vocab_size:  Optional, if there are more words than this, then prune the infrequent ones.
                            Set to None for no limit.
    :param freq_min:        Optional, ignores all words with a frequency lower than this.
    :return:                Return the word as a string and its frequency as a int
    """
    # Load the aggregated dictionary (or any other dictionnary in the format one line = word + frequency)
    df_dict = pd.read_csv(dict_file, sep=',', header=0, index_col=0, converters={'Frequency': float})
    df_dict = df_dict.sort_values(by=['Frequency'], ascending=False)

    # Keep only the most frequent words
    df_dict = df_dict[df_dict['Frequency'] >= freq_min]
    vocab = df_dict.index.tolist()

    # Truncate the vocab if necessary
    if max_vocab_size:
        vocab = vocab[:max_vocab_size]

    w = list(vocab)[random.randint(1, len(vocab))]

    return w, int(df_dict.loc[w]['Frequency'])


def get_vocab_sizes(root, models_path):
    """
    Return the vocabulary sizes of the models of the given folder
    -----
    By default root should be the same string as model_path
    """
    vocab_sizes = dict()

    for dirpath, subdirs, files in os.walk(models_path):

        logger.info("in " + dirpath)
        nb_models = 0
        model_found = False
        dir_label = "." + "".join(dirpath.rsplit(root))
        voc_label = dir_label + "/voc_sizes"
        voc_sizes = dict()

        for file_name in sorted(files):
            if file_name.endswith(".word2vec.csv"):
                model_found = True
                nb_models += 1
                logger.info("Processing " + file_name)
                year = utils.get_param(file_name, 'year')  # Extract the year from the name of the file
                try:
                    year = int(year[0])
                    # Faster than loading the file or than using the 'readlines' function
                    nb_lines = sum(1 for _ in open(os.path.join(dirpath, file_name)))
                    voc_sizes[int(year)] = nb_lines - 1
                except (IndexError, ValueError):
                    logging.warning(file_name + ": wrong year format in the file name.")
                if not nb_models % 10:
                    print(str(nb_models) + " models processed")
        if model_found:
            if voc_sizes:
                vocab_sizes[voc_label] = voc_sizes
            print(dir_label + " processed (total of " + str(nb_models) + " models)")
    return vocab_sizes
