import os


def save_log(file_name, data):
    """
    Save some data into a log file in the log folder
    -----
    :param file_name:   Name of the file where to store the metrics
    :param data:        Dictionary of the data to store. If the file does not exist, the keys are used as headers
    """
    # Get the log folder of the project
    log_path = '/'.join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1]) + "/log"
    # Create the log directory if necessary
    if not os.path.exists(log_path):
        os.makedirs(log_path)

    # Construct the path of the log file
    file_path = os.path.join(log_path, file_name)

    # If the file doesn't exist we need to add the csv header
    if not os.path.isfile(file_path):
        with open(file_path, "w+") as log_file:
            log_file.write(';'.join(data.keys()) + "\n")
    # Write the line in the file
    with open(file_path, "a+") as log_file:
        log_file.write(';'.join([str(d).replace('.', ',') for d in data.values()]) + "\n")
