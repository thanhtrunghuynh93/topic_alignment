"""
clustering.py:
Contains useful functions to help with the vector space clustering.

Copyright (C) 2018-2020 Ian Jeantet <ian.jeantet@irisa.fr>
Copyright (C) 2018 Diego Cardenas Cabeza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import time
# External
import numpy as np
import pandas as pd
import networkx as nx
import ipywidgets as widgets
# Local files
from assets.OHC.lib import utils
from assets.OHC.lib import log

# General configuration
logger = logging.getLogger()


#########################
# OHC object definition #
#########################


class Cluster(object):

    def __init__(self, nodes=None, nb_links=0, adj_nodes=None, max_neighbour_links=None, children=None, graph=None):
        """
        Initializes a Cluster object.
        -----
        :param nodes:       Set of nodes of the cluster. If None is given, an empty set will be used
        :param nb_links:    Initial number of links between the nodes of the cluster
        :param graph:       Determine the number of internal links of the set of nodes from an undirected graph if given
        """
        if nodes is None:
            # Empty Cluster
            nodes = set()
            nb_links = 0
            children = []
            adj_nodes = []
            max_neighbour_links = 0
        else:
            nodes = set(nodes)
            if graph:
                # If a graph is given we determine the number of internal links from it
                nb_links = internal_degree(graph, nodes)
                adj_nodes = sorted(adjacent_nodes(graph, nodes).items(),
                                   key=lambda kv: kv[1], reverse=True)
            if adj_nodes:
                max_neighbour_links = adj_nodes[0][1]
            else:
                adj_nodes = []
                if not max_neighbour_links:
                    max_neighbour_links = 0

            if children is None:
                children = []
            else:
                children = list(children)
        self.__nodes = nodes                                # List of nodes of the cluster
        self.__nb_links = nb_links                          # Number of internal links between the nodes
        self.__adj_nodes = adj_nodes                        # list((adj_node, nb_links)) for each adjacent node
        self.__max_neighbour_links = max_neighbour_links    # Maximal number of links to an adjacent nde
        self.__children = children                          # FEATURE add a pointer to its child clusters

    def get_nodes(self):
        return list(self.__nodes)

    def to_labels(self, labels):
        return set(labels[c] for c in self.__nodes)

    def get_nb_links(self):
        return self.__nb_links

    def get_adj_nodes(self):
        return self.__adj_nodes

    def get_max_neighbour_links(self):
        return self.__max_neighbour_links

    def get_children(self):
        return self.__children

    def set_nodes(self, nodes):
        self.__nodes = nodes

    def set_nb_links(self, nb_links):
        self.__nb_links = nb_links

    def set_adj_nodes(self, adj_nodes):
        self.__adj_nodes = adj_nodes

    def update_adj_nodes(self, n):
        adj_nodes = dict(self.__adj_nodes)
        adj_nodes[n] = adj_nodes.get(n, 0) + 1
        self.__adj_nodes = sorted(adj_nodes.items(), key=lambda kv: kv[1], reverse=True)

    def set_max_neighbour_links(self, nb_links):
        self.__max_neighbour_links = nb_links

    def increment_nb_links(self):
        self.__nb_links += 1
        return self

    def density(self):
        """
        Compute the density of the cluster
        -----
        :return:    The density as a float
        """
        if len(self.__nodes) <= 1:
            return 1.0
        else:
            return float(2 * self.__nb_links) / (len(self.__nodes) * (len(self.__nodes) - 1))

    def __iter__(self):
        return (c for c in self.get_nodes())

    def __str__(self):
        return str(self.__nodes)

    def __len__(self):
        return len(self.__nodes)

    def __eq__(self, other):
        return set(self) == set(other)

    def __hash__(self):
        return object.__hash__(self)

    def display(self):
        print("Cluster " + str(self.__nodes) + ":")
        print(" - " + str(self.__nb_links) + " link(s)")
        print(" - max_neighbour_links = " + str(self.__max_neighbour_links))


class Node(object):

    def __init__(self, cluster, d_min=None, d_max=None, density_min=None, density_max=None):
        """
        Initialize a Node.
        -----
        :param cluster:     A Cluster object
        :param d_min:       (Optional) Distance minimal corresponding to the creation of the cluster
        :param d_max:       (Optional) Distance maximal corresponding to the complete merge of the cluster
        :param density_min: (Optional) Density of the cluster at its creation
        :param density_max: (Optional) Density of the cluster at the merge
        """
        self.__cluster = cluster
        self.__d_min = d_min
        if d_max is None:
            d_max = d_min
        self.__d_max = d_max
        self.__density_min = density_min
        if density_max is None:
            density_max = density_min
        self.__density_max = density_max

    # TODO def a "serialization" function


class OHCLevel(object):

    def __init__(self, clust_li, d_min, d_max=None, density_min=None, density_max=None):
        """
        Initialize a level of a OHC object.
        -----
        :param clust_li:    List of Cluster objects
        :param d_min:       Distance minimal corresponding to the level
        :param d_max:       (Optional) Distance maximal corresponding to the level
        :param density_min: (Optional) List of densities of the Cluster objects corresponding to the minimal distance
        :param density_max: (Optional) List of densities of the Cluster objects corresponding to the maximal distance
        """
        if d_max is None:
            d_max = d_min
        if density_min is None:
            density_min = list()
        if density_max is None:
            density_max = density_min
        self.__d_min = d_min
        self.__d_max = d_max
        self.__clusters = list(clust_li)
        self.__density_min = list(density_min)
        self.__density_max = list(density_max)

    def d_min(self):
        return self.__d_min

    def d_max(self):
        return self.__d_max

    def set_d_min(self, d):
        self.__d_min = d

    def set_d_max(self, d):
        self.__d_max = d

    def clusters(self):
        return self.__clusters

    def remove_cluster(self, cluster):
        try:
            index = [set(c) for c in self.__clusters].index(set(cluster))
            del self.__clusters[index]
            del self.__density_min[index]
            del self.__density_max[index]
        except ValueError:
            raise Warning(str(cluster) + " not in the list of clusters!")

    def __len__(self):
        return len(self.__clusters)

    def density_min(self):
        return self.__density_min

    def density_max(self):
        return self.__density_max

    def set_density_min(self, density):
        self.__density_min = density

    def set_density_max(self, density):
        self.__density_max = density

    def display(self, labels=None):
        to_print = "distance: " + str(self.__d_min)
        if self.__d_max != self.__d_min:
            to_print += " -> " + str(self.__d_max)
        to_print += "; clusters: ["
        for clust, dens_min, dens_max in zip(self.__clusters, self.__density_min, self.__density_max):
            if labels is None:
                str_clust = str(set(clust))
            else:
                str_clust = str(set([labels[c] for c in clust]))
            to_print += str_clust + "(" + str(dens_min)
            if dens_min != dens_max:
                to_print += " -> " + str(dens_max)
            to_print += "), "
        to_print = to_print[:-2] + "]"
        print(to_print)

    def display_clusters(self):
        return str(self.__clusters)


class OHC(object):

    def __init__(self, level=None, name=None, labels=None, freqs=None):
        """
        For the integrity of the hierarchy we can set only one level during the initialisation.
        -----
        :param level:   (Optional) First level of the hierarchy to create
        :type level:    OHCLevel
        :param name:    Name of the hierarchy
        :param labels:  List of labels of the elements contained in the hierarchy
        """
        if level is None:
            self.__levels = list()
        else:
            self.__levels = list([level])
        if name is None:
            self.__name = "hierarchical_structure"
        else:
            self.__name = name
        if labels is None:
            if level:
                # FEAT. add labels according to the content of __levels and not only an integer
                self.__element_info = pd.DataFrame([str(i) for i in range(len(level))], columns=["label"])
                if freqs is None:
                    self.__element_info["frequency"] = 1  # Add frequency of 1 to every nodes
                else:
                    self.__element_info["frequency"] = list(freqs)
            else:
                self.__element_info = pd.DataFrame(columns=["label"])
        else:
            self.__element_info = pd.DataFrame(list(labels), columns=["label"])
            if freqs is None:
                self.__element_info["frequency"] = 1  # Add frequency of 1 to every nodes
            else:
                self.__element_info["frequency"] = list(freqs)

    # Level related methods #
    # TODO the returned structure is not the same if size is given or not
    def levels(self, size=None):
        """
        Return the list of levels of the object optionally filtered to a given number of clusters.
        -----
        :param size:    Optional, requested size (number of clusters) of the levels to return.
        :return:        The list of levels of the object.
        """
        if size is None:
            return self.__levels
        else:
            i_min = 0
            i_max = len(self.__levels) - 1
            # Verify that the hierarchy is not empty
            if i_max < 0:
                raise ValueError('Empty hierarchy')
            level_tmp = self.__levels[i_min]
            # Verify that it is possible to have the required number of clusters in the hierarchy
            if len(level_tmp) < size:
                raise UserWarning('No level of the size ' + str(size) + ' in the hierarchy')
            else:
                levels = OHC(labels=self.labels(), freqs=self.frequencies())
                i = i_min
                while i <= i_max and len(self.__levels[i]) > size:
                    i += 1
                while i <= i_max and len(self.__levels[i]) == size:
                    levels.add_level(self.__levels[i])
                    i += 1
                return levels

    def nb_levels(self):
        return len(self.__levels)

    def __set_levels(self, ohc_levels):
        self.__levels = list(ohc_levels)

    def add_level(self, ohc_level):
        """
        Add a level to the hierarchy if the list of clusters is different from the last one present in the hierarchy.
        If the lists are the same, we update the distance max and the density max for the last level of clusters.
        -----
        :param ohc_level:   A OHCLevel object representing the level we want to add to the hierarchy.
        :type ohc_level:    OHCLevel
        :return:            Return the updated hierarchy.
        """
        if not self.__levels:
            # OHC is empty
            self.__levels = [ohc_level]
            if self.__element_info.empty:
                self.__element_info = pd.DataFrame([str(i) for i in range(len(ohc_level))], columns=["label"])
                self.__element_info["frequency"] = 1
        elif self.__levels[-1].clusters() == ohc_level.clusters():
            # Same list of cluster -> update d_max and density_max
            self.__levels[-1].set_d_max(ohc_level.d_max())
            self.__levels[-1].set_density_max(ohc_level.density_max())
        else:
            # Else add level
            self.__levels.append(ohc_level)

    def cut(self, size):
        """
        Cut the OHC object and return a coverage of the data with a given number of clusters.
        If several levels have the same size we compute the average persistence (d_death - d_birth) of their clusters
        and select the most persistent level.
        -----
        :param size:    Number of clusters
        :return:        An OHCLevel object containing the most persistent level of the correct size
        """
        i_min = 0
        i_max = len(self.__levels) - 1
        # Verify that the hierarchy is not empty
        if i_max < 0:
            raise ValueError('Empty hierarchy')

        # Look for the first level with a size not greater than the size we want by going up in the hierarchy
        i = i_max
        while i >= i_min and len(self.__levels[i]) < size:
            i -= 1

        # Get this first level
        i_level_max = i  # Level max of the correct size
        level_i = self.__levels[i]

        # We set size to the smallest value greater than size if not present in the hierarchy
        size = len(level_i)

        # Verify that it is possible to have the required number of clusters in the hierarchy
        # if len(level_i) != size:
        #    raise UserWarning('No level of the size ' + str(size) + ' in the hierarchy')

        # In the case there is only one level with the requested size
        if i-1 < i_min or len(self.__levels[i-1]) != size:
            return level_i

        # Initialize the output DataFrame with its content
        clusters_i = [set(ci) for ci in level_i.clusters()]
        candidate_clusters = pd.DataFrame({"cluster": clusters_i,
                                           "d_birth": level_i.d_min(),
                                           "d_death": level_i.d_max()})

        # Determine d_death for the first candidates by looking into the upper levels
        i_up = i_level_max+1
        clusters_common = clusters_i
        while i_up <= i_max:
            clusters_common_new = []
            level_up = self.__levels[i_up]
            clusters_up = [set(c_up) for c_up in level_up.clusters()]
            d_min_up = level_up.d_min()
            # For each cluster whom we still don't know the d_death
            for c in clusters_common:
                # we look for those who are in the upper level
                if c in clusters_up:
                    # We still don't know their d_death
                    clusters_common_new.append(c)
                else:
                    # We set their d_death to the d_min of the current upper level
                    # where they don't appear for the first time
                    candidate_clusters.at[
                        candidate_clusters.loc[candidate_clusters["cluster"] == c].index, "d_death"] = d_min_up
            # We stop the process if we didn't find any cluster of level_i in level_up (all the d_death are set)
            if not clusters_common_new:
                break
            clusters_common = clusters_common_new
            i_up += 1

        # Process all the lower levels of the same size
        i -= 1
        clusters_old = clusters_i
        while i >= i_min and len(self.__levels[i]) == size:
            level_i = self.__levels[i]
            clusters_i = [set(ci) for ci in level_i.clusters()]
            d_min = level_i.d_min()
            d_max = level_i.d_max()
            for c in clusters_i:
                # If c is not already a candidate we add it
                if c not in clusters_old:
                    candidate_clusters.loc[len(candidate_clusters)] = [c, d_min, d_max]
            for c in clusters_old:
                if c not in clusters_i:
                    candidate_clusters.at[
                        candidate_clusters.loc[candidate_clusters["cluster"] == c].index, "d_death"] = d_min
            clusters_old = clusters_i
            i -= 1
        i_level_min = i+1  # Level max of the correct size

        # Determine the d_birth of the last clusters by looking into the lower levels of the hierarchy
        i_low = i_level_min-1
        clusters_common = clusters_i
        while i_low >= i_min:
            clusters_common_new = []
            level_low = self.__levels[i_low]
            clusters_low = level_low.clusters()
            d_min_low = level_low.d_min()
            # For each cluster in the low level
            for c_low in clusters_low:
                c_low = set(c_low)
                # we look for those who are in the subset of clusters whom we still don't know the d_birth
                # (they were still present in the previous low level)
                if c_low in clusters_common:
                    # We update d_birth
                    candidate_clusters.at[candidate_clusters.loc[candidate_clusters["cluster"] == c_low].index, "d_birth"] = d_min_low
                    # And add c_low to the list of clusters of level_i that are still in the current low level
                    clusters_common_new.append(c_low)
            # We stop the process if we didn't find any cluster of level_i in level_low
            if not clusters_common_new:
                break
            clusters_common = clusters_common_new
            i_low -= 1

        # Compute the persistence (d_death - d_birth) of the candidates
        candidate_clusters["persistence"] = candidate_clusters["d_death"] - candidate_clusters["d_birth"]

        # Compute the average persistence of each level of the correct size
        level_persistence = []
        for i in range(i_level_min, i_level_max+1):
            level_i = self.__levels[i]
            clusters_i = [set(ci) for ci in level_i.clusters()]
            persistence_i = np.mean([float(candidate_clusters.loc[candidate_clusters["cluster"] == c]["persistence"]) for c in clusters_i])
            level_persistence.append(persistence_i)

        # Return a hierarchy containing the most persistent level
        i_best = i_level_min + np.argmax(level_persistence)
        return self.__levels[i_best]

    def __len__(self):
        return len(self.__levels)

    # Label related methods #

    # def labels(self):
    #     return self.__labels

    def labels(self, cluster=None, most_freq=None):
        if cluster is None:
            return list(self.__element_info["label"])
        else:
            if most_freq:
                clust_elements = self.__element_info.iloc[cluster]
                sorted_elements = clust_elements.sort_values(by=['frequency'], ascending=False)
                return list(sorted_elements[:most_freq]["label"])
            else:
                return list(self.__element_info.iloc[cluster]["label"])

    def most_freq(self, cluster_labels=None, k=None):
        if not k:
            if cluster_labels:
                return cluster_labels
            else:
                return self.__element_info["label"]
        else:
            clust_elements = self.__element_info
            if cluster_labels:
                clust_elements = clust_elements[clust_elements["label"].isin(cluster_labels)]
            sorted_elements = clust_elements.sort_values(by=['frequency'], ascending=False)
            return list(sorted_elements[:k]["label"])
    # Frequency related methods #

    def frequencies(self):
        return list(self.__element_info["frequency"])

    # Name related methods #

    def name(self):
        return self.__name

    def rename(self, name):
        self.__name = name

    # Other methods #

    def display(self):
        print("--- OHC object ---")
        print(self.__name)
        for level in self.__levels:
            level.display(labels=self.labels())
        print("------------------")

    def top_k(self, k=-1):
        """
        Return the top of the hierarchy up to a maximal number of clusters equals to k.
        -----
        :param k:   Maximal limit for the number of the clusters. Can be less if k is greater than the number of leaves
                    of if there is not level with k clusters in the hierarchy.
        :return:    A OHC object containing the top k hierarchy.
        """
        n = len(self)
        levels = OHC(name=self.__name + ".top_" + str(k), labels=self.labels(), freqs=self.frequencies())
        if k <= 0 or k >= len(self.__levels[0]):
            levels.rename(self.__name)
            levels.__set_levels(self.__levels)
        else:
            i_min = 0
            i_max = n
            i = int((i_min + i_max) / 2)
            while not (len(self.__levels[i]) == k and len(self.__levels[i-1]) > k):
                if len(self.__levels[i]) > k:
                    i_min = i
                else:
                    i_max = i
                i = int((i_min + i_max) / 2)
                # If after update i == i_min then k is not present in the hierarchy so we cut at k-1
                if i == i_min:
                    i = i_max
                    break
            levels.__set_levels(self.__levels[i:])
        return levels

    def detect_cluster_level(self, clust, i_min=0, i_max=None):
        """
        Look for the first occurrence of the smallest cluster that contains clust. Return the index of the
        corresponding level and this cluster.
        -----
        :param clust:       Cluster to look for (as a list, a set of elements or a Cluster object ).
        :param i_min:       Minimal range of the hierarchy where to look (by default this value is set to 0).
        :param i_max:       Maximal range of the hierarchy where to look.
                            (by default this value is set to the length of the hierarchy minus 1).
        :return:            Return the smallest cluster containing clust and the corresponding level index or
                            respectively -1 and None if the clust is not found.
        """
        # Check parameters
        if i_max is None:
            i_max = len(self) - 1
        s_clust = set(clust)

        # Basic case
        if i_min == i_max:
            for c in self.__levels[i_min].clusters():
                if s_clust.issubset(set(c)):
                    return i_min, c
            return -1, None

        # General case
        else:
            i = int((i_min + i_max) / 2)

            if i == i_min:  # ie i_min == i_max - 1
                for c in self.__levels[i_min].clusters():
                    if s_clust.issubset(set(c)):
                        return i_min, c
                for c in self.__levels[i_max].clusters():
                    if s_clust.issubset(set(c)):
                        return i_max, c
                return -1, None
            else:
                # For the current level
                for c in self.__levels[i].clusters():
                    # If we find a cluster whom clust is a subset we need to search in the lower part of the hierarchy
                    if s_clust.issubset(set(c)):
                        return self.detect_cluster_level(clust, i_min, i)
                # Otherwise in the upper part
                return self.detect_cluster_level(clust, i, i_max)

    def detect_branch(self, clust):
        """
        Detect the clusters included and including clust in the hierarchy. This constitutes the "branch" in which clust
        appears.
        -----
        :param clust:       Cluster to look for (as a list, set of elements or Cluster object).
        :return:            Return the branch of the hierarchy in which clust appears.
        """
        s_clust = set(clust)
        # Init the branch as an empty hierarchy
        branch = OHC(name="branch_" + self.__name, labels=self.labels(), freqs=self.frequencies())
        # For each level we add a level to the branch
        for level_tmp in self.__levels:
            # Determine the indices of the sub and super clusters
            indices_tmp = [i for i, c in enumerate(level_tmp.clusters())
                           if set(c).issubset(s_clust) or s_clust.issubset(set(c))]
            branch.add_level(OHCLevel(clust_li=[level_tmp.clusters()[j] for j in indices_tmp],
                                      d_min=level_tmp.d_min(), d_max=level_tmp.d_max(),
                                      density_min=[level_tmp.density_min()[j] for j in indices_tmp],
                                      density_max=[level_tmp.density_max()[j] for j in indices_tmp]
                                      )
                             )
        return branch

    def detect_sub_hierarchy(self, clust):
        """
        Look for the first occurrence of the smallest cluster that contains clust in the hierarchy and then determine
        the upper branch in which this cluster appears as well as the sub hierarchy starting from this cluster.
        -----
        :param clust:       Cluster to look for (as a list, set of elements or Cluster object).
        :return:            Return a part of the hierarchy containing the upper branch and the sub hierarchy of clust.
        """
        s_clust = set(clust)
        i_clust, h_clust = self.detect_cluster_level(clust)
        if i_clust >= 0:
            # Process cluster level
            level_clust = self.__levels[i_clust]
            # Detect clusters
            indices_tmp = [i for i, c in enumerate(level_clust.clusters())
                           if set(c).issubset(s_clust) or s_clust.issubset(set(c))]
            # Init sub hierarchy
            branch = [OHCLevel(clust_li=[level_clust.clusters()[j] for j in indices_tmp],
                               d_min=level_clust.d_min(), d_max=level_clust.d_max(),
                               density_min=[level_clust.density_min()[j] for j in indices_tmp],
                               density_max=[level_clust.density_max()[j] for j in indices_tmp]
                               )
                      ]

            # First part: detect sub clusters of h_clust
            for i in range(1, i_clust + 1):
                h_tmp = self.__levels[i_clust - i]
                h_old = branch[-1]
                clusters_tmp = []
                indices_tmp = []
                for i_tmp, c_tmp in enumerate(h_tmp.clusters()):
                    for c_old in h_old.clusters():
                        if set(c_tmp).issubset(set(c_old)):
                            clusters_tmp.append(c_tmp)
                            indices_tmp.append(i_tmp)

                # If we obtain the same set of clusters than the previous level we update the minimal values of distance
                # and density (we need to slice the density list to keep only those of the subset of clusters)
                if h_old.clusters() == clusters_tmp:
                    branch[-1].set_d_min(h_tmp.d_min())
                    branch[-1].set_density_min([h_tmp.density_min()[j] for j in indices_tmp])
                # Else we add a new level (here we need to slice both density lists)
                else:
                    branch.append(OHCLevel(clust_li=clusters_tmp, d_min=h_tmp.d_min(), d_max=h_tmp.d_max(),
                                           density_min=[h_tmp.density_min()[j] for j in indices_tmp],
                                           density_max=[h_tmp.density_max()[j] for j in indices_tmp]
                                           )
                                  )
            branch.reverse()

            # Second part: detect super clusters of h_clust
            for h_tmp in self.__levels[i_clust + 1:]:
                clusters_tmp = []
                indices_tmp = []
                for i, c in enumerate(h_tmp.clusters()):
                    if s_clust.issubset(set(c)):
                        clusters_tmp.append(c)
                        indices_tmp.append(i)

                if branch[-1].clusters() == clusters_tmp:
                    branch[-1].set_d_max(h_tmp.d_max())
                    branch[-1].set_density_max([h_tmp.density_max()[j] for j in indices_tmp])
                else:
                    branch.append(OHCLevel(clust_li=clusters_tmp, d_min=h_tmp.d_min(), d_max=h_tmp.d_max(),
                                           density_min=[h_tmp.density_min()[j] for j in indices_tmp],
                                           density_max=[h_tmp.density_max()[j] for j in indices_tmp]
                                           )
                                  )
            sub_hierarchy = OHC(name="sub_" + self.__name, labels=self.labels(), freqs=self.frequencies())
            sub_hierarchy.__set_levels(branch)
            return sub_hierarchy
        else:
            raise ValueError(str(s_clust) + " does not appear in the hierarchy.")

    def issubhierarchy(self, hierarchy):
        """
        Return True if the OHC object is a sub hierarchy of the hierarchy given in parameter
        -----
        :param hierarchy:   An OHC object
        :return:            True if self is in hierarchy. False otherwise
        """

        levels1 = hierarchy.levels()
        levels2 = self.__levels
        depth1 = len(levels1)
        depth2 = len(levels2)
        i1 = depth1 - 1  # Position in the hierarchy 1
        i2 = depth2 - 1  # Position in the hierarchy 2
        while i1 >= 0 and i2 >= 0:
            clusters1 = levels1[i1].clusters()
            clusters2 = levels2[i2].clusters()
            nb_clust1 = len(clusters1)
            nb_clust2 = len(clusters2)

            ##########
            # Different number of clusters #
            if nb_clust1 > nb_clust2:
                return False
            elif nb_clust1 < nb_clust2:
                i1 -= 1

            ##########
            # Same number of clusters #
            else:
                if set([frozenset(c1) for c1 in clusters1]) == set([frozenset(c2) for c2 in clusters2]):
                    i1 -= 1
                    i2 -= 1
                else:
                    i1 -= 1

        if i2 > 0:
            return False
        else:
            return True


###################
# Graph functions #
###################


def internal_degree(graph, nodes):
    """
    Compute for a given graph the number of internal links of a list of nodes.
    -----
    :param graph:   Undirected graph whom the nodes are a subset of its nodes
    :param nodes:   List of nodes of the graph
    :return:        Return the number of internal links
    """
    return graph.subgraph(nodes).number_of_edges()


# def internal_degree(graph, nodes):
#     """
#     Compute for a given graph the number of internal links of a list of nodes.
#     -----
#     Complexity:
#     - adj -> constant time?
#     - & -> O(min(len(s), len(t))) in average
#     Total -> O(len(clust)^2)
#     -----
#     :param graph:   Undirected graph whom the nodes are a subset of its nodes
#     :param nodes:   List of nodes of the graph
#     :return:        Return the number of internal links
#     """
#     if len(nodes) == 0:
#         return -1
#     elif len(nodes) == 1:
#         return 0
#     else:
#         nodes = set(nodes)
#         internal_links = 0
#         for node in nodes:
#             internal_links += len(nodes & set(graph.adj[node]))
#             # print("for " + str(c) + " in " + str(clust) + ": nb_links = " + str(nb_links))
#         return internal_links / 2


def external_degree(graph, nodes):
    """
    Compute for a given graph the number of external links of a list of nodes.
    -----
    :param graph:   Undirected graph whom the nodes are a subset of its nodes
    :param nodes:   List of nodes of the graph
    :return:        Return the number of external links
    """
    nodes = set(nodes)
    external_links = 0
    for node in nodes:
        external_links += len(set(graph.adj[node]) - nodes)
    return external_links


def subgraph_density(graph, nodes=None):
    """
    Compute the density of a set of nodes of a graph in terms of links between these nodes.
    If nodes is None, return the density of the whole graph. Nodes that are not in the graph will be (quietly) ignored.
    -----
    :param graph:   An undirected graph
    :param nodes:   A list of nodes of the graph
    :return:        The density as a float
    """
    return nx.density(graph.subgraph(nodes))


# Use subgraph_density(graph,nodes) instead.
@DeprecationWarning
def graph_density(graph, cluster):
    """
    Compute the density of a set of nodes of a graph in terms of links between these nodes.
    -----
    :param graph:   An undireched graph
    :param cluster: A list of nodes of the graph
    :return:        The density as a float
    """
    nodes = list(cluster)
    if len(nodes) < 1:
        raise ValueError(str(nodes) + ": incorrect cluster")
    elif len(nodes) == 1:
        return 1.0
    else:
        nb_links = internal_degree(graph, nodes)
        return float(2 * nb_links) / (len(nodes) * (len(nodes) - 1))


def get_connected_subgraph(graph, cluster):
    """
    Determine the connected part of the graph containing the nodes of the clusters in parameter.
    -----
    Raise an error if the elements of the cluster are not in the same connected subgraph.
    -----
    :param graph:   Undirected graph
    :param cluster: A Cluster object
    :return:        A set of nodes of the graph
    """
    nodes = list(cluster)
    connected_nodes = set()
    if len(nodes) > 0:
        node_tmp = nodes[0]  # We arbitrary take the first node of the list as starting point
        nodes_tmp = set(graph.adj[node_tmp])  # We look for all its neighbours
        connected_nodes.add(node_tmp)  # Then we can add it to the set of connected nodes
        while nodes_tmp:  # We continue to do the same until the set of neighbours is empty
            node_tmp = nodes_tmp.pop()  # Take one of the neighbours
            if node_tmp not in connected_nodes:  # We look if we already saw it or not
                nodes_tmp |= set(graph.adj[node_tmp])  # If not we update the set of neighbours
                connected_nodes.add(node_tmp)  # Then add it to the set of connected nodes
        # In the end we verify that all the nodes of the clusters are in the set of connected nodes
        if not set(nodes).issubset(connected_nodes):
            raise ValueError("The cluster " + str(nodes) + " is not a connected subgraph!")
    return connected_nodes


def break_cluster(graph, cluster):
    nodes = list(cluster)
    cluster_analysis = []

    partitions = utils.split_nodes(nodes)
    for first, second in partitions:
        d_first = graph_density(graph, first)
        d_second = graph_density(graph, second)
        d_inter = interborder_density(graph, first, second)[2]
        d_border = border_density(graph, first, second)[2]
        cluster_analysis.append([set(first), d_first, d_inter, d_border, d_second, set(second)])

    cluster_analysis_df = pd.DataFrame(cluster_analysis,
                                       columns=["Cluster1", "Density", "Intercluster density", "Interborder density",
                                                "Density", "Cluster2"])
    return cluster_analysis_df


def border(graph, clust1, clust2):
    """
    Determine the border nodes of clust1 and clust2. A node of clust1 (resp. clust2) is in the "border" if it exists a
    link between this node and a node of clust2 (resp. clust1).
    -----
    :param graph:   Undirected graph that contains clust1, clust2 and certain links between the nodes
    :param clust1:  List of nodes that represents a cluster
    :param clust2:  List of nodes that represents a cluster
    :return:        Return 2 lists of nodes. The "border" of clust1 and the "border" of clust2
    """
    border1 = set()
    border2 = set()
    for c1 in clust1:
        adj_c1_tmp = set(clust2) & set(graph.adj[c1])
        if adj_c1_tmp:
            border1.add(c1)
            border2 |= adj_c1_tmp
    return border1, border2


def intercluster_links(graph, clust1, clust2):
    """
    Determine the number of links that exists between 2 clusters in a graph.
    -----
    :param graph:   Undirected graph that contains clust1, clust2 and certain links between the nodes
    :param clust1:  List of nodes that represents a cluster
    :param clust2:  List of nodes that represents a cluster
    :return:        Return the number of links between clust1 and clust2 in the graph
    """
    nb_links = 0
    nodes1 = clust1
    nodes2 = clust2
    for c1 in nodes1:
        adj_c1_tmp = set(nodes2) & set(graph.adj[c1])
        nb_links += len(adj_c1_tmp)
    return nb_links


def outdegree_connected_subgraph(graph, cluster):
    nodes = set(cluster.get_nodes())
    connected_subgraph = get_connected_subgraph(graph, cluster)
    external_nodes = connected_subgraph - nodes
    external_links = 0
    for node in nodes:
        external_links += len(set(graph.adj[node]) & external_nodes)
    return external_links


def adjacent_nodes(graph, cluster):
    """
    Return the set of nodes that have at least a link with the given cluster in the graph.
    -----
    :param graph:   Undirected graph
    :param cluster: A Cluster object or a list of nodes of the graph
    :return:        A list of pair (node, nb_links) for each node linked to the cluster.
    """
    nodes = set(cluster)
    adj_nodes = dict()
    # For each node of cluster
    for n in nodes:
        # We look for the adjacent nodes of the graph that are not in the cluster
        for a in set(graph.adj[n]) - set(nodes):
            adj_nodes[a] = adj_nodes.get(a, 0) + 1
    return adj_nodes

#####################
# Cluster functions #
#####################


def update_cluster_density(graph, clusters):
    """
    Update the density (through the number of links) of a list of clusters according to a graph.
    -----
    Complexity:
    - get_nb_links -> O(len(clust)^2)
    Total -> O(Sum of len(clust)^2) < O(n^2)
    -----
    :param graph:       Undirected graph that contains the nodes of the clusters
    :param clusters:    List of Cluster objects to update
    """
    for clust in clusters:
        clust.set_nb_links(internal_degree(graph, clust.get_nodes()))


def adjacent_clusters(graph, clusters, cluster):
    """
    Determine the adjacent clusters of a given cluster according to the graph. Two clusters are adjacent if it exists
    at least a link between one of their nodes.
    -----
    Complexity:
    - difference operator "-" -> O(len(s))
    - for loop -> O(len(cluster)*len(adj)) < O(len(cluster)^2)
    - return -> O(Sum of len(clust)^2) < O(len(cluster)^2)
    Total < O(len(cluster)^2)
    -----
    :param graph:       Undirected graph that contains the nodes of the clusters
    :param clusters:    List of Cluster objects that are present in the graph
    :param cluster:     Cluster object whom we want to determine the adjacent clusters
    :return:            Return a list of pair index/adjacent Cluster
    """
    nodes = cluster.get_nodes()
    adjacent_nodes = set()
    # For each node of cluster
    for n in nodes:
        # We look for the adjacent nodes of the graph that are not in the cluster
        adjacent_nodes |= set(graph.adj[n]) - set(nodes)
    # Then we return the list of clusters that contains at least one of these adjacent nodes
    return [(i, clust) for i, clust in enumerate(clusters) if bool(set(clust.get_nodes()) & adjacent_nodes)]


def get_connected_clusters(graph, clusters, cluster):
    """
    Determine the set of clusters composing the connected subgraph that contains the clusters in parameter.
    -----
    Raise an error if the elements of the cluster are not in the same connected subgraph.
    -----
    :param graph:       Undirected graph that contains the nodes of the clusters
    :param clusters:    List of Cluster objects that are present in the graph
    :param cluster:     Cluster object whom we want to determine the clusters of its connected subgraph
    :return:            Return a list of pair index/adjacent Cluster
    """

    # Find the index of the cluster in the list of clusters
    cluster_index = -1
    for i, clust in enumerate(clusters):
        if set(cluster) == set(clust):
            cluster_index = i
            break
    # Raise an error if not found
    if cluster_index == -1:
        raise ValueError("The cluster " + str(set(cluster)) + " is not in the list of clusters")

    clusters_tmp = {cluster_index}  # The starting point will be the index of the cluster
    connected_clusters = set()
    while clusters_tmp:  # We loop until there is no new adjacent cluster to process
        i_tmp = clusters_tmp.pop()  # We take one of the adjacent cluster
        # print("precessing " + str(i_tmp))
        if i_tmp not in connected_clusters:  # We look if we already processed it or not
            # If not we look at its adjacent clusters and add them to the set of clusters to process
            clusters_tmp.update([pair[0] for pair in adjacent_clusters(graph, clusters, clusters[i_tmp])])
        connected_clusters.add(i_tmp)

    return [(i, clusters[i]) for i in connected_clusters]


def border_density(graph, clust1, clust2):
    """
    Determine the border of each cluster and compute the density of the cluster formed by these two borders.
    -----
    :param graph:   Undirected graph that contains clust1, clust2 and certain links between the nodes
    :param clust1:  List of nodes that represents a cluster
    :param clust2:  List of nodes that represents a cluster
    :return:        Return the density of the cluster formed by the borders of clust1 and clust2
    """
    border1, border2 = border(graph, clust1, clust2)
    return border1, border2, graph_density(graph, border1.union(border2))


def interborder_density(graph, clust1, clust2):
    """
    Determine the border of each cluster and compute the density between these borders.
    -----
    Complexity:
    - & -> O(min(len(s), len(t))) in average
    - for loop -> < O(len(clust1)*len(clust2))
    Total < O(len(clust1)*len(clust2))
    -----
    :param graph:   Undirected graph that contains clust1, clust2 and certain links between the nodes
    :param clust1:  List of nodes that represents a cluster
    :param clust2:  List of nodes that represents a cluster
    :return:        Return the density between the borders of clust1 and clust2
    """
    border1 = set()
    border2 = set()
    inter_links = 0
    # Determine the borders of the 2 clusters (ie the part that are interlinked between the 2 clusters)
    # and the number of links in between
    for c1 in set(clust1):
        adj_c1_tmp = set(clust2) & set(graph.adj[c1])
        inter_links += len(adj_c1_tmp)
        if adj_c1_tmp:
            border1.add(c1)
            border2 |= adj_c1_tmp

    return border1, border2, float(inter_links) / (len(border1) * len(border2) - len(set(border1) & set(border2)))


def intercluster_density(graph, cu, cv):
    nodes_u = list(cu)
    nodes_v = list(cv)
    inter_links = intercluster_links(graph, nodes_u, nodes_v)
    return float(inter_links) / (len(nodes_u) * len(nodes_v) - len(set(nodes_u) & set(nodes_v)))


#################
# OHC functions #
#################


def to_merge(graph, cu, cv, l):
    """
    Determine which parts of the Clusters cu and cv need to be merged according to the merging criterion l
    -----
    Complexity:
    - interborder_density -> < O(len(clust1)*len(clust2))
    - Constant time for the rest
    Total -> < O(len(clust1)*len(clust2))
    -----
    :param graph:   Undirected graph that contains the nodes of the Cluster objects
    :param cu:      First Cluster to look at
    :param cv:      Second Cluster to look at
    :param l:       Merging criterion. If the difference if density is less than l we can merge some parts of the
                    clusters
    :return:        Return the pair of sets of nodes to merge or None is nothing needs to be merged
    """
    # Compute the density of the 2 clusters involved
    d_u = cu.density()
    d_v = cv.density()
    border_u, border_v, d_u_v = interborder_density(graph, cu, cv)
    nodes_u = cu.get_nodes()
    nodes_v = cv.get_nodes()
    # # Determine the borders of the 2 clusters (ie the part that are interlinked between the 2 clusters)
    # border_u, border_v = border(graph, nodes_u, nodes_v)
    # # Determine the intercluster density
    # inter_links = intercluster_links(graph, border_u, border_v)
    # d_u_v = float(inter_links) / (len(border_u) * len(border_v)
    #                               - len(set(border_u) & set(border_v)))
    # Comparison of the different computed densities
    if abs(d_u - d_u_v) < l:
        if abs(d_v - d_u_v) < l:
            # Complete merge between cu and cv
            return set(nodes_u), set(nodes_v)
        else:
            # Merge cu and border_v
            return set(nodes_u), set(border_v)
    elif abs(d_v - d_u_v) < l:
        # Merge cv and border_u
        return set(border_u), set(nodes_v)
    else:
        return None


def hierarchical_clustering_with_overlapping(data, merging_criterion=0.1, batch_size=1, normalize=True,
                                             preprocessed_hierarchy=None, name=None, labels=None, freqs=None,
                                             wid_container=None, logs=None):
    """
    Perform an agglomerative fuzzy clustering on a given distance matrix. This algorithm works by looking at the
    distances in an increasing order.
    -----
    :param data:                    Either a distance matrix or a sorted list of tuples
    :param merging_criterion:       Merging criterion that will allow 2 clusters to merge partially or completely during
                                    the process
    :param batch_size:              Size of the batch of distances that we need to take in count before looking for any
                                    cluster modifications
    :param normalize:               Whether or not the distances should be normalized
    :param preprocessed_hierarchy:  A dictionary containing the necessary elements to initialize the hierarchical
                                    clustering to a specific state (graph, clusters, hierarchy and starting_distance)
    :param name:                    Name of the hierarchy
    :param labels:                  List of labels of the elements contained in the hierarchy (leave elements)
    :param freqs:                   List of frequencies of the elements contained in the hierarchy (leave elements)
    :param wid_container:           Container where to display the progresses and results. Default to None = standard
                                    output
    :param logs:                    Log some information during the processing of the algo
    :return:                        Return the built hierarchy which is a list of levels where each level is a list of
                                    clusters associated to an interval of distances
    """

    # Init logs #
    #############

    if logs is None:
        logs = dict()

    time_start = time.process_time()
    ts = time.gmtime()
    log_file_name = "hierarchy_time_" + time.strftime("%s", ts) + ".csv"
    logs["batch_size"] = batch_size

    # Constants and variables #
    ###########################

    every = 1000

    # Counter variables
    nb_tuples = 0
    nb_impacted_clusters = 0
    # TODO verity or change the value of modified clusters to correspond to the number of different clusters
    #  in the hierarchy
    nb_modified_clusters = 0
    nb_batchs = 0
    nb_merges = 0

    # For the hierarchy computation
    d_old = 0
    d = 0
    d_min = 0
    d_max = 0
    current_batch = 0  # Will represent the number of different distances we saw by processing the tuples one by one
    impacted_clusters = set()

    # Distance matrix pre-processing #
    ##################################

    try:
        nb_vectors = data.shape[0]
        # From the distance matrix to a sorted list of tuples
        li = utils.matrix_to_tuples(data, triangle=True)
        sorted_li = sorted(li, key=utils.tuple_distance)
    except AttributeError:
        # If it's not a matrix we suppose that a list of tuples was given
        sorted_li = data
        nb_vectors = int((1+(8*len(sorted_li) + 1)**.5)/2)

    if len(sorted_li) > 0 and normalize:
        # min_dist = get_distance(sorted_li[0])
        min_dist = 0
        norm_factor = utils.tuple_distance(sorted_li[-1]) - min_dist
        if norm_factor != 1:
            sorted_li = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_li]

    # Object initialisation #
    #########################

    if name is None:
        if preprocessed_hierarchy:
            hierarchy_name = preprocessed_hierarchy["hierarchy"].name()
        else:
            hierarchy_name = "hierarchical_structure"
    else:
        hierarchy_name = name
    hierarchy_name += ".merge_" + str(merging_criterion).replace('.', '_') + ".batch_" + str(batch_size) + ".ohc"

    if preprocessed_hierarchy:
        # Init graph
        distance_graph = preprocessed_hierarchy["graph"]
        # Init cluster list
        clusters = preprocessed_hierarchy["clusters"]
        # Init hierarchy
        hierarchy = preprocessed_hierarchy["hierarchy"]
        hierarchy.rename(hierarchy_name)
        # Cut the list of tuples
        starting_distance = preprocessed_hierarchy["distance"]

        while sorted_li and utils.tuple_distance(sorted_li[0]) <= starting_distance:
            sorted_li.pop(0)
        # Update hierarchy computation variables
        d_old = starting_distance
        d = starting_distance
        d_min = starting_distance
        d_max = starting_distance

    else:
        # Init graph
        distance_graph = nx.Graph(name='hierarchy_generation_graph')
        distance_graph.add_nodes_from([i for i in range(nb_vectors)])
        # Init cluster list
        clusters = [Cluster(nodes=[n]) for n in distance_graph.nodes]
        # Init hierarchy
        hierarchy = OHC(OHCLevel([c.get_nodes() for c in clusters], d_min=d_min,
                                 density_min=[c.density() for c in clusters]),
                        name=hierarchy_name, labels=labels, freqs=freqs)

    # Init widgets #
    ################

    if wid_container:
        wid_time = widgets.Button(description="0s")
        wid_modified_clusters = widgets.Button(description="0")
        wid_clusters = widgets.Button(description=str(len(clusters)))
        wid_hierarchy = widgets.Button(description="1")
        wid_log = widgets.HTML(value="")
        wid_progress = widgets.VBox([widgets.HBox([widgets.Button(description="Time"), wid_time]),
                                     widgets.HBox([widgets.Button(description="Modified clusters"), wid_modified_clusters]),
                                     widgets.HBox([widgets.Button(description="Number of clusters"), wid_clusters]),
                                     widgets.HBox([widgets.Button(description="Number of levels"), wid_hierarchy]),
                                     wid_log
                                     ])

    # Hierarchy computation #
    #########################

    # Add links
    for tu in utils.log_progress(sorted_li, every=every, name='Tuples', wid_container=wid_container):
        nb_tuples += 1

        d = utils.tuple_distance(tu)

        # When we reach the batch size we need to be sure that we took in count all the links for the last distance
        if current_batch >= batch_size and d != d_old:

            # Batch statistics
            nb_batchs += 1
            nb_impacted_clusters += len(impacted_clusters)

            # Reinitialize the variables
            current_batch = 0

            # Update the impacted clusters
            # update_cluster_density(distance_graph, [clusters[c] for c in list(impacted_clusters)])

            # We try to grow the impacted clusters by adding the most linked adjacent nodes
            # if they keep the same density
            # Clusters to delete from the list of clusters as they are merged with another one
            modified_cluster_indices = set()
            # List of unchanged clusters
            unchanged_cluster_indices = set()
            # New clusters we will compute
            new_clusters = set()
            for i_imp in list(impacted_clusters):

                # Info of the impacted cluster
                clust_tmp = clusters[i_imp]     # Cluster object
                nodes_tmp = set(clust_tmp)      # Set of nodes

                # We look if the current cluster is a subset of a already computed new cluster
                # Not a good idea if we add lot of links that have the same distance (ie at the same time)
                # sub = False
                # for c in new_clusters:
                #     if nodes_tmp.issubset(set(c)):
                #         modified_cluster_indices.add(i_imp)
                #         nb_merges += 1
                #         sub = True
                #         # FEATURE update c children
                #         break

                # We process it only if it's not the case
                # if not sub:

                # Info of the impacted cluster
                nb_links_tmp = clust_tmp.get_nb_links()             # Nb of internal links of the cluster
                clust_density_tmp = clust_tmp.density()             # Internal density of the cluster
                adj_nodes_tmp = clust_tmp.get_adj_nodes().copy()    # list((adj_node, nb_links))
                max_neighbour_links_tmp = clust_tmp.get_max_neighbour_links()

                # We look for a new cluster only if the maximal number of links to a neighbour node increased
                if adj_nodes_tmp and adj_nodes_tmp[0][1] >= max_neighbour_links_tmp:
                    while adj_nodes_tmp:
                        # Copy of the adjacent nodes in case they don't change
                        adj_nodes_save = adj_nodes_tmp.copy()

                        # We look for the set of nodes that are the most linked with clust_tmp
                        n_tmp = adj_nodes_tmp.pop(0)    # Current adjacent node
                        adj_nb_links = n_tmp[1]         # Associated adjacent links
                        nodes_to_add = [n_tmp[0]]
                        while adj_nodes_tmp:
                            n_tmp = adj_nodes_tmp.pop(0)
                            if n_tmp[1] == adj_nb_links:
                                nodes_to_add.append(n_tmp[0])
                            else:
                                break

                        # New cluster candidate
                        new_clust_nb_links = nb_links_tmp + len(nodes_to_add) * adj_nb_links \
                                             + internal_degree(distance_graph, nodes_to_add)
                        new_nb_nodes = len(nodes_tmp) + len(nodes_to_add)
                        new_clust_density = float(2 * new_clust_nb_links) / (new_nb_nodes * (new_nb_nodes - 1))

                        # Validation?
                        # FEATURE Always compare the new density to the density of the impacted cluster even if it
                        #  already grew?
                        if abs(clust_density_tmp - new_clust_density) <= merging_criterion:
                            # Update the temporary cluster with the new nodes
                            nodes_tmp.update(nodes_to_add)
                            nb_links_tmp = new_clust_nb_links
                            clust_density_tmp = float(2 * nb_links_tmp) / (len(nodes_tmp) * (len(nodes_tmp) - 1))

                            # If we added nodes to the cluster we start the process again
                            adj_nodes_tmp = sorted(adjacent_nodes(distance_graph, nodes_tmp).items(),
                                                   key=lambda kv: kv[1], reverse=True)

                            if not adj_nodes_tmp:
                                # If there is no adjacent node to the new cluster
                                max_neighbour_links_tmp = 0

                            # Else we break the loop
                        else:
                            # The nodes with adj_nb_links were not added to the cluster so adj_nb_links becomes the
                            # max number of links to and adjacent node of the cluster
                            max_neighbour_links_tmp = adj_nb_links
                            # And we restore the adj_nodes to the save
                            adj_nodes_tmp = adj_nodes_save
                            break

                    # We create a new cluster if the set of nodes has changed
                    if nodes_tmp != set(clust_tmp):
                        modified_cluster_indices.add(i_imp)

                        # Look for all unchanged clusters if it is a subset if the new cluster
                        # and add it to the list of modified clusters if it is the case
                        mod_clust_tmp = {i for i in unchanged_cluster_indices
                                         if set(clusters[i]).issubset(nodes_tmp)}
                        nb_merges += len(mod_clust_tmp)
                        unchanged_cluster_indices.difference_update(mod_clust_tmp)
                        modified_cluster_indices.update(mod_clust_tmp)

                        # Look if the new created cluster is not a subset of another new cluster
                        sub = False
                        for c in new_clusters:
                            if nodes_tmp.issubset(set(c)):
                                sub = True
                                break
                        # If no we will add it to the list of new clusters
                        if not sub:
                            # Remove the new clusters included in the current new cluster
                            new_clusters = set(c for c in new_clusters if not set(c).issubset(nodes_tmp))
                            # Add the new cluster to the list of new clusters
                            # FEATURE add the children to the new cluster
                            new_clusters.add(Cluster(nodes=nodes_tmp, nb_links=nb_links_tmp, adj_nodes=adj_nodes_tmp))
                    else:
                        # We update the maximal number of links to an adjacent node for the unmodified cluster
                        clusters[i_imp].set_max_neighbour_links(max_neighbour_links_tmp)
                        unchanged_cluster_indices.add(i_imp)

            # Statistics
            nb_modified_clusters += len(modified_cluster_indices)

            # Clean the list of clusters
            # Remove the sub clusters from the list
            for index in sorted(list(modified_cluster_indices), reverse=True):
                # print("- " + str(clusters[index].get_nodes()))
                del clusters[index]
            # Look if a non impacted clusters is a subset of a new_clusters
            for nc in new_clusters:
                clusters = [c for c in clusters if not set(c).issubset(set(nc))]

            # Add the new clusters to the list of clusters
            clusters.extend(new_clusters)

            # Update the hierarchy
            hierarchy.add_level(OHCLevel(clust_li=[c.get_nodes() for c in clusters],
                                         d_min=d_min, d_max=d_max,
                                         density_min=[c.density() for c in clusters]
                                         )
                                )
            # If we have only one cluster, we can stop to add links in the graph, nothing will change anymore
            if len(clusters) == 1:
                # TODO compute density_max for the last level
                hierarchy.add_level(OHCLevel(clust_li=[c.get_nodes() for c in clusters],
                                             d_min=d_min, d_max=utils.tuple_distance(sorted_li[-1]),
                                             density_min=[c.get_nodes() for c in clusters],
                                             density_max=[1]
                                             )
                                    )
                break

        if current_batch == 0:
            d_min = d
            impacted_clusters = set()

        # Update graph
        u = tu[0]
        v = tu[1]
        w = tu[2]
        distance_graph.add_edge(u, v, weight=w)

        # Determine the impacted clusters
        # impacted_clusters.update(set(i for i, c in enumerate(clusters) if u in c.get_nodes() or v in c.get_nodes()))
        for i, c in enumerate(clusters):
            if u in c.get_nodes():
                impacted_clusters.add(i)
                if v in c.get_nodes():
                    c.set_nb_links(c.get_nb_links() + 1)
                else:
                    c.update_adj_nodes(v)
            elif v in c.get_nodes():
                impacted_clusters.add(i)
                c.update_adj_nodes(u)

        # We increment i that represents the number of different seen distances for the current batch
        if d != d_old:
            current_batch += 1
            d_old = d
            if current_batch == batch_size:
                d_max = d

        # Logs
        if nb_tuples == 1 and wid_container:
            widget_content = list(wid_container.children)
            widget_content.append(wid_progress)
            wid_container.children = widget_content

        if not nb_tuples % every:
            log_data = dict()
            log_data["nb_tuples"] = nb_tuples
            log_data["time"] = time.process_time() - time_start
            log_data["modified_clusters"] = nb_modified_clusters
            log_data["nb_clusters"] = len(clusters)
            log_data["hierarchy_size"] = len(hierarchy)
            log.save_log(log_file_name, log_data)

            if wid_container:
                wid_time.description = "%.2f" % round(time.process_time() - time_start, 2) + "s"
                wid_modified_clusters.description = str(nb_modified_clusters)
                wid_clusters.description = str(len(clusters))
                wid_hierarchy.description = str(len(hierarchy))
            else:
                print(str(nb_tuples) + " tuples processed in %.2f" % round(time.process_time() - time_start, 2) + "s")
                print("-> " + str(nb_modified_clusters) + " modified clusters")
                print("-> " + str(len(clusters)) + " clusters in the "
                      + str(len(hierarchy)) + "th level of the hierarchy")

    # Ending hierarchy #
    ####################

    # If we didn't break the loop because we reached a unique cluster we need to add a last level to the hierarchy
    # for the last incomplete batch
    if current_batch != 0:

        # Batch statistics
        nb_batchs += 1
        nb_impacted_clusters += len(impacted_clusters)

        d_max = d

        # Update the impacted clusters
        # update_cluster_density(distance_graph, [clusters[c] for c in list(impacted_clusters)])

        # We try to grow the impacted clusters by adding the most linked adjacent nodes
        # if they keep the same density
        # Clusters to delete from the list of clusters as they are merged with another one
        modified_cluster_indices = set()
        # List of unchanged clusters
        unchanged_cluster_indices = set()
        # New clusters we will compute
        new_clusters = set()
        for i_imp in list(impacted_clusters):

            # Info of the impacted cluster
            clust_tmp = clusters[i_imp]  # Cluster object
            nodes_tmp = set(clust_tmp)  # Set of nodes

            # We look if the current cluster is a subset of a already computed new cluster
            # sub = False
            # for c in new_clusters:
            #     if nodes_tmp.issubset(set(c)):
            #         modified_cluster_indices.add(i_imp)
            #         nb_merges += 1
            #         sub = True
            #         # FEATURE update c children
            #         break

            # We process it only if it's not the case
            # if not sub:

            # Info of the impacted cluster
            nb_links_tmp = clust_tmp.get_nb_links()  # Nb of internal links of the cluster
            clust_density_tmp = clust_tmp.density()  # Internal density of the cluster
            adj_nodes_tmp = clust_tmp.get_adj_nodes().copy()  # list((adj_node, nb_links))
            max_neighbour_links_tmp = clust_tmp.get_max_neighbour_links()

            # We look for a new cluster only if the maximal number of links to a neighbour node increased
            if adj_nodes_tmp and adj_nodes_tmp[0][1] >= max_neighbour_links_tmp:
                while adj_nodes_tmp:
                    # Copy of the adjacent nodes in case they don't change
                    adj_nodes_save = adj_nodes_tmp.copy()

                    # We look for the set of nodes that are the most linked with clust_tmp
                    n_tmp = adj_nodes_tmp.pop(0)  # Current adjacent node
                    adj_nb_links = n_tmp[1]  # Associated adjacent links
                    nodes_to_add = [n_tmp[0]]
                    while adj_nodes_tmp:
                        n_tmp = adj_nodes_tmp.pop(0)
                        if n_tmp[1] == adj_nb_links:
                            nodes_to_add.append(n_tmp[0])
                        else:
                            break

                    # New cluster candidate
                    new_clust_nb_links = nb_links_tmp + len(nodes_to_add) * adj_nb_links \
                                         + internal_degree(distance_graph, nodes_to_add)
                    new_nb_nodes = len(nodes_tmp) + len(nodes_to_add)
                    new_clust_density = float(2 * new_clust_nb_links) / (new_nb_nodes * (new_nb_nodes - 1))

                    # Validation?
                    # FEATURE Always compare the new density to the density of the impacted cluster even if it
                    #  already grew?
                    if abs(clust_density_tmp - new_clust_density) <= merging_criterion:
                        # Update the temporary cluster with the new nodes
                        nodes_tmp.update(nodes_to_add)
                        nb_links_tmp = new_clust_nb_links
                        clust_density_tmp = float(2 * nb_links_tmp) / (len(nodes_tmp) * (len(nodes_tmp) - 1))

                        # If we added nodes to the cluster we start the process again
                        adj_nodes_tmp = sorted(adjacent_nodes(distance_graph, nodes_tmp).items(),
                                               key=lambda kv: kv[1], reverse=True)

                        if not adj_nodes_tmp:
                            # If there is no adjacent node to the new cluster
                            max_neighbour_links_tmp = 0

                        # Else we break the loop
                    else:
                        # The nodes with adj_nb_links were not added to the cluster so adj_nb_links becomes the
                        # max number of links to and adjacent node of the cluster
                        max_neighbour_links_tmp = adj_nb_links
                        # And we restore the adj_nodes to the save
                        adj_nodes_tmp = adj_nodes_save
                        break

                # We create a new cluster if the set of nodes has changed
                if nodes_tmp != set(clust_tmp):
                    modified_cluster_indices.add(i_imp)

                    # Look for all unchanged clusters if it is a subset if the new cluster
                    # and add it to the list of modified clusters if it is the case
                    mod_clust_tmp = {i for i in unchanged_cluster_indices
                                     if set(clusters[i]).issubset(nodes_tmp)}
                    nb_merges += len(mod_clust_tmp)
                    unchanged_cluster_indices.difference_update(mod_clust_tmp)
                    modified_cluster_indices.update(mod_clust_tmp)

                    # Look if the new created cluster is not a subset of another new cluster
                    sub = False
                    for c in new_clusters:
                        if nodes_tmp.issubset(set(c)):
                            sub = True
                            break
                    # If no we will add it to the list of new clusters
                    if not sub:
                        # Remove the new clusters included in the current new cluster
                        new_clusters = set(c for c in new_clusters if not set(c).issubset(nodes_tmp))
                        # Add the new cluster to the list of new clusters
                        # FEATURE add the children to the new cluster
                        new_clusters.add(Cluster(nodes=nodes_tmp, nb_links=nb_links_tmp, adj_nodes=adj_nodes_tmp))
                else:
                    # We update the maximal number of links to an adjacent node for the unmodified cluster
                    clusters[i_imp].set_max_neighbour_links(max_neighbour_links_tmp)
                    unchanged_cluster_indices.add(i_imp)

        # Statistics
        nb_modified_clusters += len(modified_cluster_indices)

        # Clean the list of clusters
        # Remove the sub clusters from the list
        for index in sorted(list(modified_cluster_indices), reverse=True):
            # print("- " + str(clusters[index].get_nodes()))
            del clusters[index]
        # Look if a non impacted clusters is a subset of a new_clusters
        for nc in new_clusters:
            clusters = [c for c in clusters if not set(c).issubset(set(nc))]

        # Add the new clusters to the list of clusters
        clusters.extend(new_clusters)

        # Update the hierarchy
        hierarchy.add_level(OHCLevel(clust_li=[c.get_nodes() for c in clusters],
                                     d_min=d_min, d_max=d_max,
                                     density_min=[c.density() for c in clusters]
                                     )
                            )

    # Ending logs #
    ###############

    time_stop = time.process_time()
    log_data = dict()
    log_data["nb_tuples"] = nb_tuples
    log_data["time"] = time_stop - time_start
    log_data["modified_clusters"] = nb_modified_clusters
    log_data["nb_clusters"] = len(clusters)
    log_data["hierarchy_size"] = len(hierarchy)
    log.save_log(log_file_name, log_data)

    if wid_container:
        wid_time.description = "%.2f" % round(time.process_time() - time_start, 2) + "s"
        wid_modified_clusters.description = str(nb_modified_clusters)
        wid_clusters.description = str(len(clusters))
        wid_hierarchy.description = str(len(hierarchy))

        wid_log_content = ""
        if nb_batchs > 0:
            wid_log_content += "Average of " + str(nb_impacted_clusters / nb_batchs) + " impacted clusters per batch<br>"
        else:
            wid_log_content += "No batch for this process<br>"
        wid_log_content += "Logs saved in " + log_file_name
        wid_log.value = wid_log_content
    else:
        print(str(nb_tuples) + " tuples processed in %.2f" % round(time.process_time() - time_start, 2) + "s")
        print("-> " + str(nb_modified_clusters) + " modified clusters")
        print("-> " + str(len(clusters)) + " clusters in the "
              + str(len(hierarchy)) + "th level of the hierarchy")
        if nb_batchs > 0:
            print("Average of " + str(nb_impacted_clusters / nb_batchs) + " impacted clusters per batch")
        else:
            print("No batch for this process")
        print("Logs saved in " + log_file_name)

    logs["nb_tuples"] = nb_tuples
    logs["time"] = time_stop - time_start
    if nb_batchs > 0:
        logs["avg_impacted_clusters"] = nb_impacted_clusters / nb_batchs
    else:
        logs["avg_impacted_clusters"] = 1

    return hierarchy


def preprocess_hierarchy(data, d_threshold, normalize=True, name=None, labels=None, freqs=None, wid_container=None):
    """
    Preprocess the hierarchy by computing the cliques on the graph containing certain links according to d_threshold.
    -----
    :param data:            Either a distance matrix or a sorted list of tuples
    :param d_threshold:     Fraction of links/distances to take in count (0 => 0 distances, 1 => all distances)
    :param normalize:       Whether of not dealing with normalized distances
    :param name:            Name of the hierarchy
    :param labels:          List of labels of the elements contained in the hierarchy
    :param wid_container:   Container where to display the progresses and results. Default to None = standard output
    :return:                Return an object usable by the hierarchy computation algorithm
                            (will init the process to this returned state)
    """

    time_start = time.process_time()

    # # Get info on the distance matrix
    # mat_size = distance_matrix.shape[0]

    # Init ph_graph
    if name is None:
        prep_name = "hierarchical_structure"
    else:
        prep_name = name
    prep_name += ".preprocessed_" + str(d_threshold).replace('.', '_')
    ph_graph = nx.Graph(name='preprocess_hierarchy_graph')

    # Distance matrix pre-processing #
    ##################################

    try:
        nb_vectors = data.shape[0]
        # From the distance matrix to a sorted list of tuples
        li = utils.matrix_to_tuples(data, triangle=True)
        sorted_li = sorted(li, key=utils.tuple_distance)
    except AttributeError:
        # If it's not a matrix we suppose that a list of tuples was given
        sorted_li = data
        nb_vectors = int((1+(8*len(sorted_li) + 1)**.5)/2)

    if len(sorted_li) > 0 and normalize:
        # min_dist = get_distance(sorted_li[0])
        min_dist = 0
        norm_factor = utils.tuple_distance(sorted_li[-1]) - min_dist
        if norm_factor != 1:
            sorted_li = [(a, b, float(d-min_dist)/norm_factor) for (a, b, d) in sorted_li]

    # Add the nodes to the graph
    ph_graph.add_nodes_from([i for i in range(nb_vectors)])

    # Add the edges to the graph according to d_threshold
    max_added_distance = 0
    while sorted_li and utils.tuple_distance(sorted_li[0]) <= d_threshold:
        (i, j, max_added_distance) = sorted_li.pop(0)
        ph_graph.add_edge(i, j, weight=max_added_distance)

    # Compute the cliques of ph_graph
    cliques = nx.find_cliques(ph_graph)
    cliques = list(cliques)

    # From cliques to cluster objects
    clusters = [Cluster(nodes=c, graph=ph_graph) for c in list(cliques)]

    # Init hierarchy
    hierarchy = OHC(OHCLevel([c.get_nodes() for c in clusters], d_min=0, d_max=max_added_distance,
                             density_min=[c.density() for c in clusters]), name=prep_name, labels=labels, freqs=freqs)

    utils.to_print("Hierarchy preprocessed in " + str(time.process_time() - time_start) + "s -> " + str(len(clusters))
                   + " cliques found", wid_container)

    return {"graph": ph_graph, "clusters": clusters, "hierarchy": hierarchy, "data": sorted_li,
            "distance": max_added_distance}


# FEATURE add the possibility to compute a feature for each node during the graph computation
# TODO integrate the changes induced by the modifications on hierarchy_to_graph function
def to_graph(hierarchy, name=None, wid_container=None):
    """
    Convert a hierarchy with a level point of view to a graph with a cluster point of view.
    -----
    Properties of the graph:
    - Graph: directed, weighted, labeled
    - Node: id, cluster (as a set of labels), d_min, d_max
    -----
    :param hierarchy:       OHC object
    :param df_model:        Word2Vec model as a Dataframe
    :param name:            (Optional) Name of the graph. If None the name is constructed from the name of the hierarchy
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return the computed networkx graph
    """
    # Create the directed graph
    if name:
        graph_name = name
    else:
        graph_name = '.'.join(str(hierarchy.name()).split('.')[:-1]) + ".graph"
    graph_hierarchy = nx.DiGraph(name=graph_name, labels=hierarchy.labels(), frequencies=hierarchy.frequencies())

    levels = hierarchy.levels()
    level0 = levels[0]
    d_min0 = level0.d_min()
    d_max0 = level0.d_max()
    clusts_old = dict()  # Indices of the clusters of the previous level

    nb_nodes = 0  # Id given to each different node of the hierarchy

    # Add the cluster nodes of the leaves of the hierarchy to the graph
    for c in level0.clusters():
        c_labelled = hierarchy.labels(c)
        graph_hierarchy.add_node(nb_nodes, cluster=c_labelled, d_min=d_min0, d_max=d_max0, occurrence=1)
        clusts_old[nb_nodes] = c_labelled
        nb_nodes += 1

    # Process the remaining levels in a bottom up fashion
    for level in utils.log_progress(levels[1:], every=10, name="Processed levels", wid_container=wid_container):
        d_min = level.d_min()
        d_max = level.d_max()
        clusts_new = dict()
        for c in level.clusters():
            c_labelled = hierarchy.labels(c)
            try:
                # Look if the current cluster already exists in the graph (if yes it has to be among the cluster of the
                # previous level ie in clusts_old)
                index = list(clusts_old.keys())[list(clusts_old.values()).index(c_labelled)]
            except ValueError:
                index = -1
            if index == -1:
                # If not we add a new node
                graph_hierarchy.add_node(nb_nodes, cluster=c_labelled, d_min=d_min, d_max=d_max, occurrence=1)
                clusts_new[nb_nodes] = c_labelled
                # And the links to the clusters of the previous level (clusts_old)
                for i_old, c_old in clusts_old.items():
                    inter = set(c_old).intersection(c_labelled)
                    if inter:
                        w = float(len(inter)) / (len(c) + len(c_old) - len(inter))
                        graph_hierarchy.add_edge(i_old, nb_nodes, weight=w)
                nb_nodes += 1
            else:
                graph_hierarchy.nodes[index]['occurrence'] += 1
                graph_hierarchy.nodes[index]['d_max'] = d_max
                clusts_new[index] = c_labelled
        clusts_removed = set(clusts_old.keys()) - set(clusts_new.keys())
        for i in clusts_removed:
            graph_hierarchy.nodes[i]['d_max'] = d_min
        clusts_old = clusts_new

    return graph_hierarchy


#########
# utils #
#########
######################
# Analysis functions #
######################
########################
# Deprecated functions #
########################