"""
lib.py:

Copyright (C) 2019-2020 Ian Jeantet <ian.jeantet@irisa.fr>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import pickle
import subprocess
import pandas as pd
import networkx as nx
import torch
import numpy as np
import graphviz
import math
import ipywidgets as widgets
from sklearn.preprocessing import normalize
import shutil

from assets.OHC.lib.utils import barycenter, save_to_pickle, load_from_pickle, to_print, log_progress, get_param
from assets.OHC.lib.embedding import load_model_from_csv
from assets.OHC.lib.metric import jaccard_similarity
from assets.FAGCN import FAGCN
from assets.VecMap.embeddings import read


def Laplacian_graph(A):
    """
    Normalized Laplacian matrix calculator.
    -----
    :param A:   Matrix to normalize
    :return:    The normalized matrix
    """
    for i in range(len(A)):
        A[i, i] = 1
    A = torch.FloatTensor(A)
    D_ = torch.diag(torch.sum(A, 0) ** (-0.5))
    A_hat = torch.matmul(torch.matmul(D_, A), D_)
    A_hat = A_hat.float()
    indices = torch.nonzero(A_hat).t()
    values = A_hat[indices[0], indices[1]]
    A_hat = torch.sparse.FloatTensor(indices, values, A_hat.size())
    return A_hat


def align_hierarchies(hierarchies, models, epochs=1000, save_path="", embedding_alignment="vecmap", reuse=True,
                      period=None, top_k=-1, wid_container=None):

    # Constants #
    #############

    # Temporary emb.txt file
    exp_path = "./"
    exp_name = "tmp"
    if not os.path.exists(os.path.join(exp_path, exp_name)):
        os.makedirs(os.path.join(exp_path, exp_name))
    exp_id = "hierarchy_alignment"
    # Input files
    source_emb_txt = os.path.join(exp_path, exp_name, "source.emb.txt")
    target_emb_txt = os.path.join(exp_path, exp_name, "target.emb.txt")
    train_dict = os.path.join(exp_path, exp_name, "train_dict.txt")
    # Output files
    src_mapped_emb = os.path.join(exp_path, exp_name, "source_mapped.emb.txt")
    trg_mapped_emb = os.path.join(exp_path, exp_name, "target_mapped.emb.txt")

    nb_hierarchies = len(hierarchies)

    # Variables #
    #############

    i1 = 0
    i2 = 1
    hierarchy_source = None
    df_source_model = None
    alignment_loaded = True

    # Output values
    alignments = []
    accuracies = []
    alignment_labels = []

    # Init #
    ########

#    if nb_hierarchies > 0:
#        # Load the first hierarchy
#        with open(hierarchies[0], "rb") as openfile:
#            hierarchy_source = pickle.load(openfile)
#
#        # Load the first model
#        df_source_model = load_model_from_csv(models[i1], sort=False)
#        df_source_model = df_source_model.iloc[:, 0:-1]  # Remove the frequency column
#
#        # Generate first VecMap input files
#        with open(source_emb_txt, 'w') as output_file:
#            output_file.write(str(df_source_model.shape[0]) + " " + str(df_source_model.shape[1]) + "\n")
#            df_source_model.to_csv(output_file, sep=' ', index=True, header=False)
#            # print(source_emb_txt + " saved")

    # Init widgets #
    ################

    if wid_container:
        wid_tab_content = []
        wid_tab = widgets.Tab(children=wid_tab_content)  # One tab per alignment
        wid_nb_alignments = widgets.Button(description="0")
        wid_counters = widgets.HBox([widgets.Button(description="Alignments"), wid_nb_alignments])
        wid_container.children = [wid_tab, wid_counters]

    # Alignment #
    ############

    while i2 < nb_hierarchies:

        # Init a new widget for the current alignment #
        ###############################################

        if wid_container:
            wid_alignment_content = []
            wid_alignment = widgets.VBox(children=wid_alignment_content)  # For the current hierarchy
            wid_tab_content.append(wid_alignment)
            wid_tab.children = wid_tab_content
            wid_tab.set_title(str(i2-1), str(i2))
        else:
            wid_alignment = None
            print("\n")
            print("--------------- Alignment " + str(i2) + " ---------------")

        # Precomputation verification #
        ###############################

        h1_name = '.'.join(hierarchies[i1].split('/')[-1].split('.')[:-2])
        h2_name = '.'.join(hierarchies[i2].split('/')[-1].split('.')[:-2])
        if wid_container:
            to_print("From <i>" + h1_name + "</i>", wid_alignment)
            to_print("to <i>" + h2_name + "</i>", wid_alignment)
        else:
            print("From " + h1_name)
            print("to " + h2_name)

        alignment_name = h1_name + "." + h2_name + ".emb_align_" + embedding_alignment.lower()
        if top_k > 0:
            alignment_name += ".top_levels_" + str(top_k)
        alignment_name += ".alignment"
        alignment_file = os.path.join(save_path, alignment_name + ".pickle")
        if save_path and reuse and os.path.isfile(alignment_file):
            # Load the alignment instead of recomputing it
            alignment = load_from_pickle(alignment_file)
            # aligned_dict = alignment["aligned_dict"]
            accuracy = alignment["emb_alignment_acc"]
            alignment_label = alignment["period_source"] + " -> " + alignment["period_target"]
            if wid_container:
                to_print("Alignment found and loaded (<i>" + alignment_name + ".pickle</i>)", wid_alignment)
            else:
                print("Alignment found and loaded (" + alignment_name + ".pickle)")

            alignment_loaded = True
        else:

            # Compute the alignment label
            h1_period = str(i1)
            h2_period = str(i2)
            if period:
                period_values_h1 = get_param(hierarchies[i1], period)
                if period_values_h1:
                    h1_period = "-".join([str(pv) for pv in period_values_h1])
                period_values_h2 = get_param(hierarchies[i2], period)
                if period_values_h2:
                    h2_period = "-".join([str(pv) for pv in period_values_h2])
            alignment_label = h1_period + " -> " + h2_period

            # Data loading (source and target) #
            ####################################

            to_print("--------------- Data loading ---------------", wid_alignment)

            # Load the source hierarchy in the case we couldn't take the previous target as the new source
            if alignment_loaded:
                with open(hierarchies[i1], "rb") as openfile:
                    hierarchy_source = pickle.load(openfile)
                # Reduce the graphs to their top k levels
                if top_k > 0:
                    hierarchy_source = top_levels(hierarchy_source, k=top_k)
            # Load the target hierarchy
            with open(hierarchies[i2], "rb") as openfile:
                hierarchy_target = pickle.load(openfile)
            # Reduce the graphs to their top k levels
            if top_k > 0:
                hierarchy_target = top_levels(hierarchy_target, k=top_k)
                to_print("Cutting graphs to " + str(top_k) + " levels:", wid_alignment)

            to_print("Source: " + str(len(hierarchy_source.nodes)) + " nodes and " + str(len(hierarchy_source.edges))
                     + " edges", wid_alignment)
            to_print("Target: " + str(len(hierarchy_target.nodes)) + " nodes and " + str(len(hierarchy_target.edges))
                     + " edges", wid_alignment)

            # Load the source model in the case we couldn't take the previous target as the new source
            if alignment_loaded:
                df_source_model = load_model_from_csv(models[i1], sort=False)
                df_source_model = df_source_model.iloc[:, 0:-1]  # Remove the frequency column
            # Load target model
            df_target_model = load_model_from_csv(models[i2], sort=False)
            df_target_model = df_target_model.iloc[:, 0:-1]  # Remove the frequency column

            if wid_container:
                to_print("'<i>" + models[i1].split('/')[-1] + "</i>' loaded -> " + str(df_source_model.shape[0])
                         + " word vectors", wid_alignment)
                to_print("'<i>" + models[i2].split('/')[-1] + "</i>' loaded -> " + str(df_target_model.shape[0])
                         + " word vectors", wid_alignment)
            else:
                print(models[i1].split('/')[-1] + " loaded -> " + str(df_source_model.shape[0]) + " word vectors")
                print(models[i2].split('/')[-1] + " loaded -> " + str(df_target_model.shape[0]) + " word vectors")

            # Embedding space alignment #
            #############################

            to_print("--------------- Embedding space alignment ---------------", wid_alignment)

            # Generate VecMap/MUSE input files
            # The source file was already saved during the previous alignment except if it was loaded and not computed
            if alignment_loaded:
                with open(source_emb_txt, 'w') as output_file:
                    output_file.write(str(df_source_model.shape[0]) + " " + str(df_source_model.shape[1]) + "\n")
                    df_source_model.to_csv(output_file, sep=' ', index=True, header=False)
                    to_print(source_emb_txt + " saved", wid_alignment)
            # target file
            with open(target_emb_txt, 'w') as output_file:
                output_file.write(str(df_target_model.shape[0]) + " " + str(df_target_model.shape[1]) + "\n")
                df_target_model.to_csv(output_file, sep=' ', index=True, header=False)
                to_print(target_emb_txt + " saved", wid_alignment)

            # Train.dict
            vocab1 = set(df_source_model.index)
            vocab2 = set(df_target_model.index)

            commom = round(100 * len(vocab1.intersection(vocab2)) / len(vocab1.union(vocab2)), 2)
            if wid_container:
                to_print(str(len(vocab1.intersection(vocab2))) + "/" + str(
                    len(vocab1.union(vocab2))) + " identical words (<b>%.2f" % commom + "%</b>)",
                         wid_alignment)
            else:
                print(str(len(vocab1.intersection(vocab2))) + "/" + str(len(vocab1.union(vocab2)))
                      + " identical words (%.2f" % commom + "%)")

            with open(train_dict, 'w') as output_file:
                for w in vocab1.intersection(vocab2):
                    output_file.write(w + " " + w + "\n")
                to_print(train_dict + " saved", wid_alignment)

            # VecMap #
            if embedding_alignment.lower() == "vecmap":

                to_print("Using VecMap", wid_alignment)

                # TODO make the subprocess work with a real relative path to map_embeddings.py
                mapping = subprocess.check_output(['python3', './assets/VecMap/map_embeddings.py', '--supervised',
                                                   train_dict, source_emb_txt, target_emb_txt, src_mapped_emb,
                                                   trg_mapped_emb])
                if mapping:
                    to_print(mapping.decode("utf-8"), wid_alignment)

            # MUSE #
            elif embedding_alignment.lower() == "muse":

                to_print("Using MUSE", wid_alignment)

                mapping = subprocess.check_output(['python3', './assets/MUSE/supervised.py', '--cuda', 'False',
                                                   '--src_lang', 'source_mapped.emb',
                                                   '--tgt_lang', 'target_mapped.emb',
                                                   '--src_emb', source_emb_txt,
                                                   '--tgt_emb', target_emb_txt,
                                                   '--exp_path', exp_path,
                                                   '--exp_name', exp_name,
                                                   '--exp_id', exp_id,
                                                   '--n_refinement', '5',
                                                   '--dico_train', train_dict,
                                                   '--dico_eval', train_dict
                                                   ])
                if mapping:
                    to_print(mapping.decode("utf-8"), wid_alignment)

                # Move and renames the results to match the output files the the rest of the process
                shutil.move(os.path.join(exp_path, exp_name, exp_id, "vectors-source_mapped.emb.txt"), src_mapped_emb)
                shutil.move(os.path.join(exp_path, exp_name, exp_id, "vectors-target_mapped.emb.txt"), trg_mapped_emb)

                # Remove other output files
                shutil.rmtree(os.path.join(exp_path, exp_name, exp_id))

            # Embedding alignment evaluation #
            coverage = subprocess.check_output(['python3', './assets/VecMap/eval_translation.py',
                                                src_mapped_emb, trg_mapped_emb, '-d', train_dict])\
                .decode("utf-8")
            accuracy = float(coverage.split(' ')[-1].split(' ')[-1].split(':')[-1][:-2])
            if wid_container:
                to_print("<b>" + coverage + "</b>", wid_alignment)
            else:
                print(coverage)

            # Vector normalization #
            ########################

            to_print("--------------- Vector normalization ---------------", wid_alignment)

            # Read input embeddings
            srcfile = open(src_mapped_emb, errors='surrogateescape')
            src_words, x = read(srcfile)
            df_source_mapped_emb = pd.DataFrame(x, index=src_words)

            trgfile = open(trg_mapped_emb, errors='surrogateescape')
            trg_words, z = read(trgfile)
            df_target_mapped_emb = pd.DataFrame(z, index=trg_words)

            df_source_norm = pd.DataFrame(normalize(df_source_mapped_emb), index=df_source_mapped_emb.index)
            to_print(str(len(src_words)) + " source vectors normalized", wid_alignment)
            df_target_norm = pd.DataFrame(normalize(df_target_mapped_emb), index=df_target_mapped_emb.index)
            to_print(str(len(trg_words)) + " target vectors normalized", wid_alignment)

            # Feature computation #
            #######################

            to_print("--------------- Feature computation ---------------", wid_alignment)

            # Get the hierarchy nodes
            nodes_info_source = pd.DataFrame(list(dict(hierarchy_source.nodes(data=True)).values()))
            nodes_info_target = pd.DataFrame(list(dict(hierarchy_target.nodes(data=True)).values()))

            # Compute the features
            features_s = [barycenter(df_source_norm.loc[list(c)]) for c in nodes_info_source["cluster"]]
            to_print("Source hierarchy: done", wid_alignment)
            features_t = [barycenter(df_target_norm.loc[list(c)]) for c in nodes_info_target["cluster"]]
            to_print("Target hierarchy: done", wid_alignment)

            to_print("Feature dimension: " + str(len(features_s[0])), wid_alignment)  # Feature_dim 300

            # Graph preprocessing #
            #######################

            to_print("--------------- Graph preprocessing ---------------", wid_alignment)

            # Compute the adj matrices
            adj_s = nx.adjacency_matrix(hierarchy_source).todense()
            adj_t = nx.adjacency_matrix(hierarchy_target).todense()
            adj_s[adj_s > 0] = 1
            adj_t[adj_t > 0] = 1
            adj_s += adj_s.T
            adj_t += adj_t.T

            # Normalized Laplacian matrix computation
            source_A_hat = Laplacian_graph(adj_s)
            target_A_hat = Laplacian_graph(adj_t)

            to_print("Done", wid_alignment)

            # Hierarchy alignment #
            #######################

            wid_alignment_acc_content = None
            if wid_container:
                wid_alignment_acc_content = widgets.VBox(layout=widgets.Layout(overflow='scroll', height='80px'))
                wid_alignment_acc = widgets.Accordion(children=[wid_alignment_acc_content], selected_index = None)
                wid_alignment_acc.set_title(0, "--------------- Hierarchy alignment ---------------")
                wid_alignment_content = list(wid_alignment.children)
                wid_alignment_content.append(wid_alignment_acc)
                wid_alignment.children = wid_alignment_content
            else:
                print("--------------- Hierarchy alignment ---------------")

            # TODO I had to set self.cuda = False in the lib file -> add a paramter?
            align_model = FAGCN.FAGCN(np.array(features_s), np.array(features_t), source_A_hat, target_A_hat,
                                      num_epochs=epochs, learning_rate=0.001)
            # TODO I redirected the FAGCN output as it's crashing my notebook (too many lines to display)
            wid_dummy = widgets.VBox()
            similarity_matrix = align_model.align(wid_container=wid_dummy)
            # print(similarity_matrix.shape())

            # Create diction of aligned pairs
            aligned_dict = {i: similarity_matrix[i].argmax() for i in range(len(hierarchy_source.nodes))}
            alignment = {"hierarchy_source": h1_name,
                         "period_source": h1_period,
                         "hierarchy_target": h2_name,
                         "period_target": h2_period,
                         "emb_alignment_alg": embedding_alignment,
                         "emb_alignment_acc": accuracy,
                         "top_k": top_k,
                         "id2clusters_s": [set(cs) for cs in nodes_info_source["cluster"]],
                         "id2clusters_t": [set(ct) for ct in nodes_info_target["cluster"]],
                         "aligned_dict": aligned_dict}

            if save_path:
                save_to_pickle(save_path, alignment, name=alignment_name, wid_container=wid_alignment)
                # print("Alignment saved in " + alignment_name)

            alignment_loaded = False

        # Add the alignment dictionary to the list of alignments
        alignments.append(alignment)
        accuracies.append(accuracy)
        alignment_labels.append(alignment_label)
        if wid_container:
            wid_nb_alignments.description = str(len(alignments))

        # Avoid some computation by moving the targets that will become the new sources
        if not alignment_loaded:
            hierarchy_source = hierarchy_target
            df_source_model = df_target_model
            os.rename(target_emb_txt, source_emb_txt)

        i1 += 1
        i2 += 1

    return alignments, accuracies, alignment_labels


# TODO modify the container management
def hierarchy_alignment(ohc1, ohc2, model1, model2, top_k=-1, epochs=1000, embedding_alignment="vecmap", period=None,
                        wid_container=None):

    # Constants #
    #############

    # Temporary emb.txt file
    exp_path = "./"
    exp_name = "tmp"
    if not os.path.exists(os.path.join(exp_path, exp_name)):
        os.makedirs(os.path.join(exp_path, exp_name))
    exp_id = "hierarchy_alignment"
    # Input files
    source_emb_txt = os.path.join(exp_path, exp_name, "source.emb.txt")
    target_emb_txt = os.path.join(exp_path, exp_name, "target.emb.txt")
    train_dict = os.path.join(exp_path, exp_name, "train_dict.txt")
    # Output files
    src_mapped_emb = os.path.join(exp_path, exp_name, "source_mapped.emb.txt")
    trg_mapped_emb = os.path.join(exp_path, exp_name, "target_mapped.emb.txt")

    # Alignment #
    ############

    # Init widget for the current alignment #
    #########################################

    if wid_container:
        wid_alignment_content = []
        wid_alignment = widgets.VBox(children=wid_alignment_content)  # For the current hierarchy
        wid_container_content = list(wid_container.children)
        wid_container_content.append(wid_alignment)
        wid_container.children = wid_container_content
    else:
        wid_alignment = None
        print("\n")
        print("--------------- Alignment ---------------")

    # Precomputation verification #
    ###############################

    h1_name = '.'.join(ohc1.split('/')[-1].split('.')[:-2])
    h2_name = '.'.join(ohc2.split('/')[-1].split('.')[:-2])
    if wid_container:
        to_print("From <i>" + h1_name + "</i>", wid_alignment)
        to_print("to <i>" + h2_name + "</i>", wid_alignment)
    else:
        print("From " + h1_name)
        print("to " + h2_name)

    alignment_name = h1_name + "." + h2_name + ".alignment"

    # Compute the alignment label
    h1_period = ""
    h2_period = ""
    if period:
        period_values_h1 = get_param(ohc1, period)
        if period_values_h1:
            h1_period = "-".join([str(pv) for pv in period_values_h1])
        period_values_h2 = get_param(ohc2, period)
        if period_values_h2:
            h2_period = "-".join([str(pv) for pv in period_values_h2])

    # Data loading (source and target) #
    ####################################

    to_print("--------------- Data loading ---------------", wid_alignment)

    # Load the source hierarchy in the case we couldn't take the previous target as the new source
    with open(ohc1, "rb") as openfile:
        hierarchy_source = pickle.load(openfile)
    # Load the target hierarchy
    with open(ohc2, "rb") as openfile:
        hierarchy_target = pickle.load(openfile)

    to_print("Source: " + str(len(hierarchy_source.nodes)) + " nodes and " + str(len(hierarchy_source.edges))
             + " edges", wid_alignment)
    to_print("Target: " + str(len(hierarchy_target.nodes)) + " nodes and " + str(len(hierarchy_target.edges))
             + " edges", wid_alignment)

    # Reduce the graphs to their top k levels
    if top_k > 0:
        hierarchy_source = top_levels(hierarchy_source, k=top_k)
        hierarchy_target = top_levels(hierarchy_target, k=top_k)
        to_print("Cutting graphs to " + str(top_k) + " levels:", wid_alignment)
        to_print("Source: " + str(len(hierarchy_source.nodes)) + " nodes and " + str(len(hierarchy_source.edges))
                 + " edges", wid_alignment)
        to_print("Target: " + str(len(hierarchy_target.nodes)) + " nodes and " + str(len(hierarchy_target.edges))
                 + " edges", wid_alignment)

    # Load the source model in the case we couldn't take the previous target as the new source
    df_source_model = load_model_from_csv(model1, sort=False)
    df_source_model = df_source_model.iloc[:, 0:-1]  # Remove the frequency column
    # Load target model
    df_target_model = load_model_from_csv(model2, sort=False)
    df_target_model = df_target_model.iloc[:, 0:-1]  # Remove the frequency column

    if wid_container:
        to_print("'<i>" + model1.split('/')[-1] + "</i>' loaded -> " + str(df_source_model.shape[0])
                 + " word vectors", wid_alignment)
        to_print("'<i>" + model2.split('/')[-1] + "</i>' loaded -> " + str(df_target_model.shape[0])
                 + " word vectors", wid_alignment)
    else:
        print(model1.split('/')[-1] + " loaded -> " + str(df_source_model.shape[0]) + " word vectors")
        print(model2.split('/')[-1] + " loaded -> " + str(df_target_model.shape[0]) + " word vectors")

    # Embedding space alignment #
    #############################

    to_print("--------------- Embedding space alignment ---------------", wid_alignment)

    # Generate VecMap/MUSE input files
    # The source file was already saved during the previous alignment except if it was loaded and not computed
    with open(source_emb_txt, 'w') as output_file:
        output_file.write(str(df_source_model.shape[0]) + " " + str(df_source_model.shape[1]) + "\n")
        df_source_model.to_csv(output_file, sep=' ', index=True, header=False)
        to_print(source_emb_txt + " saved", wid_alignment)
    # target file
    with open(target_emb_txt, 'w') as output_file:
        output_file.write(str(df_target_model.shape[0]) + " " + str(df_target_model.shape[1]) + "\n")
        df_target_model.to_csv(output_file, sep=' ', index=True, header=False)
        to_print(target_emb_txt + " saved", wid_alignment)

    # Train.dict
    vocab1 = set(df_source_model.index)
    vocab2 = set(df_target_model.index)

    commom = round(100 * len(vocab1.intersection(vocab2)) / len(vocab1.union(vocab2)), 2)
    if wid_container:
        to_print(str(len(vocab1.intersection(vocab2))) + "/" + str(
            len(vocab1.union(vocab2))) + " identical words (<b>%.2f" % commom + "%</b>)",
                 wid_alignment)
    else:
        print(str(len(vocab1.intersection(vocab2))) + "/" + str(len(vocab1.union(vocab2)))
              + " identical words (%.2f" % commom + "%)")

    with open(train_dict, 'w') as output_file:
        for w in vocab1.intersection(vocab2):
            output_file.write(w + " " + w + "\n")
        to_print(train_dict + " saved", wid_alignment)

    # VecMap #
    if embedding_alignment.lower() == "vecmap":

        to_print("Using VecMap", wid_alignment)

        # TODO make the subprocess work with a real relative path to map_embeddings.py
        mapping = subprocess.check_output(['python3', './assets/VecMap/map_embeddings.py', '--supervised',
                                           train_dict, source_emb_txt, target_emb_txt, src_mapped_emb,
                                           trg_mapped_emb])
        if mapping:
            to_print(mapping.decode("utf-8"), wid_alignment)

    # MUSE #
    elif embedding_alignment.lower() == "muse":

        to_print("Using MUSE", wid_alignment)

        mapping = subprocess.check_output(['python3', './assets/MUSE/supervised.py', '--cuda', 'False',
                                           '--src_lang', 'source_mapped.emb',
                                           '--tgt_lang', 'target_mapped.emb',
                                           '--src_emb', source_emb_txt,
                                           '--tgt_emb', target_emb_txt,
                                           '--exp_path', exp_path,
                                           '--exp_name', exp_name,
                                           '--exp_id', exp_id,
                                           '--n_refinement', '5',
                                           '--dico_train', train_dict,
                                           '--dico_eval', train_dict
                                           ])
        if mapping:
            to_print(mapping.decode("utf-8"), wid_alignment)

        # Move and renames the results to match the output files the the rest of the process
        shutil.move(os.path.join(exp_path, exp_name, exp_id, "vectors-source_mapped.emb.txt"), src_mapped_emb)
        shutil.move(os.path.join(exp_path, exp_name, exp_id, "vectors-target_mapped.emb.txt"), trg_mapped_emb)

        # Remove other output files
        shutil.rmtree(os.path.join(exp_path, exp_name, exp_id))

    else:
        raise ValueError("Please chose between VecMap and MUSE")

    # Embedding alignment evaluation #
    coverage = subprocess.check_output(['python3', './assets/VecMap/eval_translation.py',
                                        src_mapped_emb, trg_mapped_emb, '-d', train_dict]) \
        .decode("utf-8")
    accuracy = float(coverage.split(' ')[-1].split(' ')[-1].split(':')[-1][:-2])
    if wid_container:
        to_print("<b>" + coverage + "</b>", wid_alignment)
    else:
        print(coverage)

    # Vector normalization #
    ########################

    to_print("--------------- Vector normalization ---------------", wid_alignment)

    # Read input embeddings
    srcfile = open(src_mapped_emb, errors='surrogateescape')
    src_words, x = read(srcfile)
    df_source_mapped_emb = pd.DataFrame(x, index=src_words)

    trgfile = open(trg_mapped_emb, errors='surrogateescape')
    trg_words, z = read(trgfile)
    df_target_mapped_emb = pd.DataFrame(z, index=trg_words)

    df_source_norm = pd.DataFrame(normalize(df_source_mapped_emb), index=df_source_mapped_emb.index)
    to_print(str(len(src_words)) + " source vectors normalized", wid_alignment)
    df_target_norm = pd.DataFrame(normalize(df_target_mapped_emb), index=df_target_mapped_emb.index)
    to_print(str(len(trg_words)) + " target vectors normalized", wid_alignment)

    # Feature computation #
    #######################

    to_print("--------------- Feature computation ---------------", wid_alignment)

    # Get the hierarchy nodes
    nodes_info_source = pd.DataFrame(list(dict(hierarchy_source.nodes(data=True)).values()))
    nodes_info_target = pd.DataFrame(list(dict(hierarchy_target.nodes(data=True)).values()))

    # Compute the features
    features_s = [barycenter(df_source_norm.loc[list(c)]) for c in nodes_info_source["cluster"]]
    to_print("Source hierarchy: done", wid_alignment)
    features_t = [barycenter(df_target_norm.loc[list(c)]) for c in nodes_info_target["cluster"]]
    to_print("Target hierarchy: done", wid_alignment)

    to_print("Feature dimension: " + str(len(features_s[0])), wid_alignment)  # Feature_dim 300

    # Graph preprocessing #
    #######################

    to_print("--------------- Graph preprocessing ---------------", wid_alignment)

    # Compute the adj matrices
    adj_s = nx.adjacency_matrix(hierarchy_source).todense()
    adj_t = nx.adjacency_matrix(hierarchy_target).todense()
    adj_s[adj_s > 0] = 1
    adj_t[adj_t > 0] = 1
    adj_s += adj_s.T
    adj_t += adj_t.T

    # Normalized Laplacian matrix computation
    source_A_hat = Laplacian_graph(adj_s)
    target_A_hat = Laplacian_graph(adj_t)

    to_print("Done", wid_alignment)

    # Hierarchy alignment #
    #######################

    wid_alignment_acc_content = None
    if wid_container:
        wid_alignment_acc_content = widgets.VBox(layout=widgets.Layout(overflow='scroll', height='80px'))
        wid_alignment_acc = widgets.Accordion(children=[wid_alignment_acc_content], selected_index=None)
        wid_alignment_acc.set_title(0, "--------------- Hierarchy alignment ---------------")
        wid_alignment_content = list(wid_alignment.children)
        wid_alignment_content.append(wid_alignment_acc)
        wid_alignment.children = wid_alignment_content
    else:
        print("--------------- Hierarchy alignment ---------------")

    # TODO I had to set self.cuda = False in the lib file -> add a paramter?
    align_model = FAGCN.FAGCN(np.array(features_s), np.array(features_t), source_A_hat, target_A_hat,
                              num_epochs=epochs, learning_rate=0.001)
    similarity_matrix = align_model.align(wid_container=wid_alignment_acc_content)

    # Create diction of aligned pairs
    aligned_dict = {i: similarity_matrix[i].argmax() for i in range(len(hierarchy_source.nodes))}

    return {"hierarchy_source": h1_name,
            "period_source": h1_period,
            "hierarchy_target": h2_name,
            "period_target": h2_period,
            "emb_alignment_alg": embedding_alignment,
            "emb_alignment_acc": accuracy,
            "id2clusters_s": [set(cs) for cs in nodes_info_source["cluster"]],
            "id2clusters_t": [set(ct) for ct in nodes_info_target["cluster"]],
            "aligned_dict": aligned_dict}


def top_levels(ohc_graph, k):
    """
    Return a subgraph containing only nodes from the top k levels of the hierarchy
    -----
    :param ohc_graph:   The hierarchy as a networkx graph
    :param k:           Number of wanted levels
    :return:            Return the subgraph as a networkx graph
    """
    # import pdb
    # pdb.set_trace()
    # level_thresholds = ohc_graph.graph["level_thresholds"]
    level_thresholds = ohc_graph.graph["frequencies"]
    if k >= len(level_thresholds):
        return ohc_graph
    else:
        d_min = level_thresholds[-k]
        nodes_to_remove = [n[0] for n in ohc_graph.nodes(data=True) if n[1]["d_death"] <= d_min]
        ohc_copy = ohc_graph.copy()
        ohc_copy.remove_nodes_from(nodes_to_remove)
    return ohc_copy


def detect_cluster(graph, cluster):
    """
    Look for the node that contains the most number of words of the cluster given in parameter,
    walking down the hierarchy from its root
    -----
    :param graph:   The hierarchy as a networkx graph with specific nodes
    :param cluster: Bag of words
    :return:        Return the best match node found in the graph
    """
    # Extract node attributes
    nodes_info = pd.DataFrame(list(dict(graph.nodes(data=True)).values()), index=graph.nodes())
    nodes_info = nodes_info.sort_values(by=['d_birth'], ascending=True)
    nb_nodes = nodes_info.shape[0]
    root = nodes_info.iloc[-1]
    # print(root)

    # Adapt the bag of word to the vocab of the graph
    adapted_clust = set(cluster).intersection(root["cluster"])
    n = len(adapted_clust)
    # print(adapted_clust)
    # If no common word found
    if n == 0:
        return -1, dict()

    node_index = nodes_info.index[-1]
    best_candidate = root
    if len(root["cluster"]) == n:
        return node_index, best_candidate
    else:
        while 1:
            node_tmp = None
            children = list(graph.predecessors(node_index))
            for child_index in children:
                if adapted_clust.issubset(graph.nodes[child_index]["cluster"]):
                    node_index = child_index
                    node_tmp = graph.nodes[child_index]
                    break
            if not node_tmp:
                return node_index, best_candidate
            else:
                best_candidate = node_tmp


def future_evolution(node, hierarchies, alignments):
    """
    Use the cluster_index in parameter to follows its evolution through the hierarchies by using the alignments.
    -----
    :param node:            Node of hierarchies[0]
    :param hierarchies:     List of n hierarchy files
    :param alignments:      List of n-1 associating dictionaries
    :return:                Return a list of nodes containing the evolution of the cluster with one node per period
    """

    cluster_evolution = [node]
    if alignments:
        node_first = node

        a0 = alignments[0]
        index_source = a0["id2clusters_s"].index(set(node_first["cluster"]))
        # Following the alignments
        for a, h in zip(alignments, hierarchies[1:]):
            # Get the new node index and the corresponding cluster
            index_target = a["aligned_dict"][index_source]
            cluster_target = a["id2clusters_t"][index_target]
            # Get the new node from the corresponding hierarchy
            with open(h, "rb") as openfile:
                h_target = pickle.load(openfile)
            dict_clusters_t = {key: set(value) for key, value in nx.get_node_attributes(h_target, "cluster").items()}
            node_index_t = list(dict_clusters_t.keys())[list(dict_clusters_t.values()).index(cluster_target)]
            # Add the new node to the cluster evolution
            cluster_evolution.append(h_target.nodes[node_index_t])

            index_source = index_target

    return cluster_evolution


def past_evolution(node, hierarchies, alignments):
    """
    Use the cluster_index in parameter to follows its evolution through the hierarchies by using the alignments but in
    the reversed order by selecting the most similar source
    -----
    :param node:            Node of hierarchies[-1]
    :param hierarchies:     List of n hierarchy files
    :param alignments:      List of n-1 associating dictionaries
    :return:                Return a list of nodes containing the evolution in the reversed order of the cluster
                            with one node per period
    """
    cluster_evolution = [node]
    if hierarchies:
        node_last = node

        a = alignments[-1]
        cluster_target = set(node_last["cluster"])              # Current target cluster
        index_target = a["id2clusters_t"].index(cluster_target) # Current target index
        # Following the alignments in reversed order
        for a, h in zip(reversed(alignments), reversed(hierarchies[:-1])):

            # Get all the node indices that point to the current cluster
            indices_source = [i for i, x in enumerate(list(a["aligned_dict"].values())) if x == index_target]
            if len(indices_source) == 0:
                break
            else:
                # Determine the most similar cluster
                index_source = -1
                max_sim = -1
                for i in indices_source:
                    sim_tmp = jaccard_similarity(cluster_target, a["id2clusters_s"][i])
                    if sim_tmp > max_sim:
                        index_source = i
                        max_sim = sim_tmp
                # Get the new node from the source hierarchy
                with open(h, "rb") as openfile:
                    h_source = pickle.load(openfile)
                dict_clusters_s = {key: set(value) for key, value in
                                   nx.get_node_attributes(h_source, "cluster").items()}
                node_index_s = list(dict_clusters_s.keys())[
                    list(dict_clusters_s.values()).index(a["id2clusters_s"][index_source])]
                # Add the new node to the cluster evolution
                cluster_evolution.append(h_source.nodes[node_index_s])

                index_target = index_source
                cluster_target = a["id2clusters_s"][index_source]

    return cluster_evolution


def format_label(cluster_labels):
    # Transform the label to a correct form
    n = len(cluster_labels)
    k = math.ceil(math.sqrt(n))
    c_label = ""
    index = 0
    while index + k < n:
        c_label += str(cluster_labels[index:index + k]) + "\n"
        index += k
    c_label += str(cluster_labels[index:])
    return c_label


def metro_map(cluster, hierarchies, alignments, period=None, top_k=-1, most_freq=None, save=None, graph_format='svg',
              wid_container=None):
    """
    Create a metro map representation of the best matching topics with the given bag of words and their evolutions
    through time.
    -----
    :param cluster:         Bag of words to look at in the hierarchies
    :param hierarchies:     A list of hierarchies covering consecutive periods of time
    :type hierarchies:      DiGraph hierarchies saved as pickle files
    :param alignments:      A list of alignment dictionaries computed on the hierarchies
    :param period:          Period parameter from the hierarchy names ("year", "publication_year", "month"...)
                            to add to the metro map as time axis captions
    :param top_k:           Number of top levels of the hierarchies to consider (related to the used alignments
    :param most_freq:       Maximal number of words per topic to display (select the most frequent ones)
    :param save:            If a directory path is given, save the representation and
                            display it in an external window before returning it
    :param graph_format:    Format of the graphviz output. svg by default
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return the representation as a graphviz directed graph
    """

    # TODO improve the computation by avoiding the future evolution computation once we reach a known cluster
    # TODO infer top_k from tha alignments

    # Multi-timeline extractions #
    ##############################

    n = len(hierarchies)
    df_metro_map = None
    detected_nodes = []
    # nb_stops = 0
    for i in log_progress(range(n), every=1, name="Processed timelines", wid_container=wid_container):

        # Init: find the best matching node in the ith hierarchy
        with open(hierarchies[i], "rb") as openfile:
            hi = pickle.load(openfile)
            # Reduce the graph to its top k levels
            if top_k > 0:
                hi = top_levels(hi, k=top_k)
        index_i, node_i = detect_cluster(hi, cluster)
        # print(node_i)

        if index_i < 0:
            detected_nodes.append(hash(frozenset({})))

        if index_i >= 0:

            detected_nodes.append(hash(frozenset(node_i["cluster"])))

            # Compute past evolution
            cluster_past = past_evolution(node_i, hierarchies[:i+1], alignments[:i])
            # Compute future evolution
            cluster_future = future_evolution(node_i, hierarchies[i:], alignments[i:])

            # Merge the past with the future
            cluster_evolution = pd.DataFrame(list(reversed(cluster_past[1:])) + cluster_future)
            # print(i, cluster_evolution)

            if df_metro_map is None:
                # Initialize the metro map by creating the indices of the cluster and by adding their child index
                df_metro_map = cluster_evolution
                diff = n - len(df_metro_map)
                df_metro_map["period"] = [i + diff for i in range(len(df_metro_map))]
                df_metro_map["appearance"] = [1 for i in range(len(df_metro_map))]
                df_metro_map["index"] = [str(hash(frozenset(c))) + "_" + str(i + diff) for i, c in
                                         enumerate(df_metro_map["cluster"])]
                df_metro_map["next"] = list(df_metro_map["index"])[1:] + [None]

            else:
                diff = n - len(cluster_evolution)
                id_old = ""
                for j, c in enumerate(list(cluster_evolution["cluster"])):
                    id_new = str(hash(frozenset(c))) + "_" + str(j + diff)
                    if id_old:
                        # Add child index to the previous node
                        df_metro_map.loc[df_metro_map["index"] == id_old, "next"] = id_new
                    if id_new not in list(df_metro_map["index"]):
                        new_node = dict(cluster_evolution.loc[j])
                        new_node["period"] = j + diff
                        new_node["appearance"] = 1
                        new_node["index"] = id_new
                        new_node["next"] = None
                        df_metro_map.loc[len(df_metro_map)] = new_node
                    else:
                        for k, cl in enumerate(list(cluster_evolution["cluster"])[j:]):
                            df_metro_map.loc[df_metro_map["index"] == str(hash(frozenset(cl))) + "_" + str(k + j + diff), "appearance"] += 1
                        break
                    id_old = id_new

#        if df_metro_map is not None:
#            print(i, len(df_metro_map))
#
#    print(df_metro_map.sort_values(by=['period']))

    # Graph generation #
    ####################

    # Create the directed graph
    graph_metro_map = graphviz.Digraph(name="metro_map." + "_".join(cluster), format=graph_format)
    graph_metro_map.graph_attr['rankdir'] = 'LR'
    nb_periods = max(df_metro_map['period'])
    next_period = nb_periods
    for i in range(nb_periods, -1, -1):

        # Create a subgraph for the current period
        with graph_metro_map.subgraph(name='sub_' + str(i)) as sub_graph_period_i:

            # Force the nodes to be at the same level
            sub_graph_period_i.graph_attr.update(rank='same')

            # Get all the node indices of the current period
            nodes_i = df_metro_map[df_metro_map['period'] == i].index

            # Add period information
            if period and len(nodes_i) > 0:
                period_values = get_param(hierarchies[i], param=period)
                if period_values:
                    period_label = "-".join([str(pv) for pv in period_values])
                    sub_graph_period_i.node(str(i), period_label, shape='box')
                    if i < nb_periods:
                        graph_metro_map.edge(str(i), str(next_period), color='black')
                next_period = i

            # print(i, len(nodes_i))

            # Compute the label of these nodes (according to the most_freq parameter)
            if most_freq:

                with open(hierarchies[i], "rb") as openfile:
                    hi = pickle.load(openfile)

                df_frequencies_hi = pd.DataFrame(zip(hi.graph["labels"], hi.graph["frequencies"]),
                                                 columns=["label", "frequency"])
                df_frequencies_hi = df_frequencies_hi.sort_values(by=['frequency'], ascending=False)

                c_labels = []
                for ci in df_metro_map.loc[nodes_i, "cluster"]:

                    sorted_elements = df_frequencies_hi[df_frequencies_hi["label"].isin(list(ci))]
                    ci_label = list(sorted_elements[:most_freq]["label"])
                    ci_len = len(ci_label)
                    ci_label = format_label(ci_label)
                    # Add the real length of the cluster if we display only a subset of it
                    if ci_len < len(ci):
                        ci_label += "\n(" + str(len(ci)) + ")"
                    c_labels.append(ci_label)

            else:
                c_labels = [format_label(list(ci)) for ci in df_metro_map[nodes_i]["cluster"]]

            # Add the labels to display to the metro map dataframe for the nodes of the current period
            df_metro_map.loc[nodes_i, "label"] = c_labels

            # Process the nodes of the current period
            for j, node in df_metro_map.iloc[nodes_i].iterrows():

                # Add the node
                if hash(frozenset(node["cluster"])) == detected_nodes[i]:
                    color = "red"
                else:
                    color = "black"
                # Appearance in [1;nb_periods] mapped to [1;9]
                # -> x in [a;b] equivalent to y = (x-a)/(b-a)*(d-c)+c in [c;d]
                col = int((node["appearance"]-1)/(nb_periods-1)*8+1)
                fillcolor = "/greys9/" + str(col)
                if col >= 6:
                    fontcolor = "white"
                else:
                    fontcolor = "black"
                sub_graph_period_i.node(node['index'], node['label'], color=color, style="filled", fillcolor=fillcolor,
                                        fontcolor=fontcolor)

                # Add the edge
                if node['next'] is not None:
                    c = node['cluster']
                    c_next = list(df_metro_map[df_metro_map['index'] == node['next']]['cluster'])[0]
                    inter = set(c).intersection(c_next)
                    # Draw the arrow with 6 color shades according to the jaccard index between the nodes.
                    # If this index drops to 0, the edge is represented as a dashed arrow
                    if inter:
                        w = float(len(inter)) / (len(c) + len(c_next) - len(inter))
                        graph_metro_map.edge(node['index'], node['next'], color=str(int((10 * w)/2)+4),
                                             colorscheme="greys9")
                        # graph_metro_map.add_edge(id_new, id_old, weight=str(int(10 * w)))
                    else:
                        graph_metro_map.edge(node['index'], node['next'], color="4", colorscheme="greys9",
                                             style="dashed")

    if save:
        output = graph_metro_map.render(directory=save, view=True, cleanup=True)
        print("Graph saved in " + output)

    return graph_metro_map


def metro_map_v2(cluster, hierarchies, alignments, period=None, most_freq=None, save=None, graph_format='svg', wid_container=None):
    """
    Create a metro map representation of the best matching topics with the given bag of words and their evolutions
    through time.
    -----
    :param cluster:         Bag of words to look at in the hierarchies
    :param hierarchies:     A list of hierarchies covering consecutive periods of time
    :type hierarchies:      DiGraph hierarchies saved as pickle files
    :param alignments:      A list of alignment dictionaries computed on the hierarchies
    :param period:          Period parameter from the hierarchy names ("year", "publication_year", "month"...)
                            to add to the metro map as time axis captions
    :param most_freq:       Maximal number of words per topic to display (select the most frequent ones)
    :param save:            If a directory path is given, save the representation and
                            display it in an external window before returning it
    :param graph_format:    Format of the graphviz output. svg by default
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return the representation as a graphviz directed graph
    """

    # TODO improve the computation by avoiding the future evolution computation once we reach a known cluster

    # Multi-timeline extractions #
    ##############################

    n = len(hierarchies)
    df_metro_map = None
    detected_nodes = []
    # nb_stops = 0
    for i in log_progress(range(n), every=1, name="Processed timelines", wid_container=wid_container):

        # Init: find the best matching node in the ith hierarchy
        with open(hierarchies[i], "rb") as openfile:
            hi = pickle.load(openfile)
        index_i, node_i = detect_cluster(hi, cluster)
        # print(node_i)

        if index_i < 0:
            detected_nodes.append(hash(frozenset({})))

        if index_i >= 0:

            detected_nodes.append(hash(frozenset(node_i["cluster"])))

            # Compute past evolution
            cluster_past = past_evolution(index_i, hierarchies[:i + 1], alignments[:i])
            # Compute future evolution
            cluster_future = future_evolution(index_i, hierarchies[i:], alignments[i:])

            # Merge the past with the future
            cluster_evolution = pd.DataFrame(list(reversed(cluster_past[1:])) + cluster_future)

            if df_metro_map is None:
                # Initialize the metro map by creating the indices of the cluster and by adding their child index
                df_metro_map = cluster_evolution
                diff = n - len(df_metro_map)
                df_metro_map["period"] = [i + diff for i in range(len(df_metro_map))]
                df_metro_map["appearance"] = [1 for i in range(len(df_metro_map))]
                df_metro_map["index"] = [str(hash(frozenset(c))) + "_" + str(i + diff) for i, c in
                                         enumerate(df_metro_map["cluster"])]
                df_metro_map["next"] = list(df_metro_map["index"])[1:] + [None]

            else:
                diff = n - len(cluster_evolution)
                id_old = ""
                for j, c in enumerate(list(cluster_evolution["cluster"])):
                    id_new = str(hash(frozenset(c))) + "_" + str(j + diff)
                    if id_old:
                        # Add child index to the previous node
                        df_metro_map.loc[df_metro_map["index"] == id_old, "next"] = id_new
                    if id_new not in list(df_metro_map["index"]):
                        new_node = dict(cluster_evolution.loc[j])
                        new_node["period"] = j + diff
                        new_node["appearance"] = 1
                        new_node["index"] = id_new
                        new_node["next"] = None
                        df_metro_map.loc[len(df_metro_map)] = new_node
                    else:
                        for k, cl in enumerate(list(cluster_evolution["cluster"])[j:]):
                            df_metro_map.loc[df_metro_map["index"] == str(hash(frozenset(cl))) + "_" + str(k + j + diff), "appearance"] += 1
                        break
                    id_old = id_new

    # Graph generation #
    ####################

    # Create the directed graph
    graph_metro_map = graphviz.Digraph(name="metro_map." + "_".join(cluster), format=graph_format)
    graph_metro_map.graph_attr['rankdir'] = 'LR'
    nb_periods = max(df_metro_map['period'])
    next_period = nb_periods
    for i in range(nb_periods, -1, -1):

        # Create a subgraph for the current period
        with graph_metro_map.subgraph(name='sub_' + str(i)) as sub_graph_period_i:

            # Force the nodes to be at the same level
            sub_graph_period_i.graph_attr.update(rank='same')

            # Get all the node indices of the current period
            nodes_i = df_metro_map[df_metro_map['period'] == i].index

            # Add period information
            if period and len(nodes_i) > 0:
                period_values = get_param(hierarchies[i], param=period)
                if period_values:
                    period_label = "-".join([str(pv) for pv in period_values])
                    sub_graph_period_i.node(str(i), period_label, shape='box')
                    if i < nb_periods:
                        graph_metro_map.edge(str(i), str(next_period), color='black')
                next_period = i

            # print(i, len(nodes_i))

            # Compute the label of these nodes (according to the most_freq parameter)
            if most_freq:

                with open(hierarchies[i], "rb") as openfile:
                    hi = pickle.load(openfile)

                df_frequencies_hi = pd.DataFrame(zip(hi.graph["labels"], hi.graph["frequencies"]),
                                                 columns=["label", "frequency"])
                df_frequencies_hi = df_frequencies_hi.sort_values(by=['frequency'], ascending=False)

                c_labels = []
                for ci in df_metro_map.loc[nodes_i, "cluster"]:

                    sorted_elements = df_frequencies_hi[df_frequencies_hi["label"].isin(list(ci))]
                    ci_label = list(sorted_elements[:most_freq]["label"])
                    ci_label = format_label(ci_label)
                    # Add the real length of the cluster if we display only a subset of it
                    if len(ci_label) < len(ci):
                        ci_label += "\n(" + str(len(ci)) + ")"
                    c_labels.append(ci_label)

            else:
                c_labels = [format_label(list(ci)) for ci in df_metro_map[nodes_i]["cluster"]]

            # Add the labels to display to the metro map dataframe for the nodes of the current period
            df_metro_map.loc[nodes_i, "label"] = c_labels

            # Process the nodes of the current period
            for j, node in df_metro_map.iloc[nodes_i].iterrows():

                # Add the node
                if hash(frozenset(node["cluster"])) == detected_nodes[i]:
                    color = "red"
                else:
                    color = "black"
                # Appearance in [1;nb_periods] mapped to [1;9]
                # -> x in [a;b] equivalent to y = (x-a)/(b-a)*(d-c)+c in [c;d]
                col = int((node["appearance"]-1)/(nb_periods-1)*8+1)
                fillcolor = "/greys9/" + str(col)
                if col >= 6:
                    fontcolor = "white"
                else:
                    fontcolor = "black"
                sub_graph_period_i.node(node['index'], node['label'], color=color, style="filled", fillcolor=fillcolor,
                                        fontcolor=fontcolor)

                # Add the edge
                if node['next'] is not None:
                    c = node['cluster']
                    c_next = list(df_metro_map[df_metro_map['index'] == node['next']]['cluster'])[0]
                    inter = set(c).intersection(c_next)
                    # Draw the arrow with 6 color shades according to the jaccard index between the nodes.
                    # If this index drops to 0, the edge is represented as a dashed arrow
                    if inter:
                        w = float(len(inter)) / (len(c) + len(c_next) - len(inter))
                        graph_metro_map.edge(node['index'], node['next'], color=str(int((10 * w)/2)+4),
                                             colorscheme="greys9")
                        # graph_metro_map.add_edge(id_new, id_old, weight=str(int(10 * w)))
                    else:
                        graph_metro_map.edge(node['index'], node['next'], color="4", colorscheme="greys9",
                                             style="dashed")

    if save:
        output = graph_metro_map.render(directory=save, view=True, cleanup=True)
        print("Graph saved in " + output)

    return graph_metro_map


def metro_map_v1(cluster, hierarchies, alignments, distance_axe=True, save=None, graph_format='svg'):
    """
    Create a representation of the hierarchy with a view focused on the changes of the clusters.
    -----
    :param hierarchy:       A list of hierarchy levels where each level is composed of 3 elements, the distance min and
                            the distance max for which the clusters don't change and the associated list of clusters
    :type hierarchy:        OHC
    :param top_levels:      Number of levels to display from the top of the hierarchy
    :param labels:          List of labels for the nodes
    :param density_color:   If true, color the nodes according to the evolution of their density. Else, keep them white
    :param distance_axe:    If true, display an axe of distance and the right of the graph
    :param save:            If a directory path is given, save the representation and
                            display it in an external window before returning it
    :param graph_format:    Format of the graphviz output. svg by default
    :param wid_container:   (Optional) Widget container where to display the process information.
                            If None the standard output is used
    :return:                Return the representation as a graphviz directed graph
    """

    # Create the directed graph
    graph_metro_map = graphviz.Digraph(name="metro_map." + "_".join(cluster), format=graph_format)
    graph_metro_map.graph_attr['rankdir'] = 'LR'

    # TODO add a timeline with period names extracted from the hierarchy names
    # TODO add subgraphs to align the periods of the cluster evolutions

    n = len(hierarchies)
    for i in range(n):

        # Init: find the best matching node in the ith hierarchy
        with open(hierarchies[i], "rb") as openfile:
            hi = pickle.load(openfile)
        index_i, node_i = detect_cluster(hi, cluster)

        if index_i >= 0:

            # Compute past evolution
            cluster_past = past_evolution(index_i, hierarchies[:i + 1], alignments[:i])
            cluster_past["index"] = [-i for i in range(len(cluster_past))]
            cluster_past = cluster_past.set_index("index")
            cluster_past.index.name = None
            # Compute future evolution
            cluster_future = future_evolution(index_i, hierarchies[i:], alignments[i:])

            # Merge the past with the future
            cluster_evolution = cluster_past.iloc[::-1].iloc[:-1].append(cluster_future)
            print(cluster_evolution)

            period = n
            c_old = set()
            id_old = ""
            for c in reversed(list(cluster_evolution["cluster"])):
                # Add the node to the graph
                id_new = str(c) + "_" + str(period-1)
                graph_metro_map.node(id_new, format_label(list(c)), style="filled", gradientangle="90")
                # graph_metro_map.add_node(id_new, label=format_label(list(c)))
                # Add a link for the current cluster evolution
                if id_old:
                    inter = set(c_old).intersection(c)
                    if inter:
                        w = float(len(inter)) / (len(c) + len(c_old) - len(inter))
                        graph_metro_map.edge(id_new, id_old, weight=str(int(10 * w)), colorscheme="greys9")
                        # graph_metro_map.add_edge(id_new, id_old, weight=str(int(10 * w)))
                    else:
                        graph_metro_map.edge(id_new, id_old, weight="10", colorscheme="greys9", style="dashed")
                        # graph_metro_map.add_edge(id_new, id_old, weight="10", style="dashed")
                c_old = c
                id_old = id_new
                period -= 1

    if save:
        output = graph_metro_map.render(directory=save, view=True, cleanup=True)
        print("Graph saved in " + output)
    return graph_metro_map
